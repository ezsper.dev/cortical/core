import { ApolloError } from 'apollo-server-errors';

export class InternalServerError extends ApolloError {
  static defaultMessage = `An internal server error has occurred`;
  constructor(message?: string, originalError?: Error) {
    super(message == null ? InternalServerError.defaultMessage : message);
    if (originalError != null) {
      this.originalError = originalError;
    }
  }
}