import { ApolloError } from 'apollo-server-errors';

export class EntityNotFoundError extends ApolloError {
  constructor(entityName: string, criteria: any) {
    super(`Could not found "${entityName}" with the given criteria`, 'ENTITY_NOT_FOUND_ERROR', {
      entityName,
      criteria,
    });
  }
}
