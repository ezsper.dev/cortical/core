import { ApolloError } from 'apollo-server-errors';

export class EntityDuplicationError extends ApolloError {
  constructor(entityName: string, criteria: any) {
    super(`There's is already a record for "${entityName}" with the given criteria`, 'ENTITY_DUPLICATION_ERROR', {
      entityName,
      criteria,
    });
  }
}
