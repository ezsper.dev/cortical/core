# Cortical

This is a framework built on top of [Apollo Server](https://github.com/apollographql/apollo-server) in the same principals as [Erect](https://gitlab.com/ezsper.com/erect). It specifies a design pattern based on [GraphQL](http://graphql.org/learn/) with a [built-in transpiler](https://gitlab.com/ezsper.com/cortical/ts2graphql) of your TypeScript code to GraphQL type definitions.

# How to use it

```
npm install @cortical/core yarn -g
cortical create myapi
cd myapi
yarn install
yarn develop
```
