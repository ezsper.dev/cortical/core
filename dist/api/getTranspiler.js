"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts2graphql_1 = require("@cortical/ts2graphql");
const externalTypes_1 = require("../hooks/externalTypes");
const paths_1 = require("../paths");
let transpiler;
function getTranspiler() {
    const externalTypes = externalTypes_1.default.filter([], {});
    if (typeof transpiler === 'undefined') {
        transpiler = new ts2graphql_1.TypeScript2GraphQL({
            paths: {
                root: paths_1.getApiPath(),
                types: 'types',
                query: 'query',
                mutation: 'mutation',
                subscription: 'subscription',
                scalars: 'scalars',
                validators: 'validators',
            },
            types: externalTypes,
        });
    }
    return transpiler;
}
exports.getTranspiler = getTranspiler;
//# sourceMappingURL=getTranspiler.js.map