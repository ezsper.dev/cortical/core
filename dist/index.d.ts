/// <reference types="node" />
import '@spine/bootstrap/node';
import './loadConfig';
import * as hooks from './hooks';
import { ApolloError } from 'apollo-server-errors';
import { EntityDuplicationError } from './errors/EntityDuplicationError';
import { EntityNotFoundError } from './errors/EntityNotFoundError';
import { InternalServerError } from './errors/InternalServerError';
import { DocumentNode, ExecutionResult, GraphQLSchema, GraphQLFieldResolver, GraphQLError } from 'graphql';
import { ContextParams, BaseContext } from './api/context';
import { UrlWithParsedQuery } from 'url';
import { IncomingHttpHeaders } from 'http';
export { hooks, ExecutionResult, EntityNotFoundError, EntityDuplicationError, InternalServerError, };
export * from './api/url';
export * from './paths';
export { default as gql } from 'graphql-tag';
export { GraphQLError } from 'graphql';
export { bootstrap } from '@spine/bootstrap';
export * from 'apollo-server-errors';
export { Application, Request, Response, NextFunction, } from './server';
export { ValidatorMap, Resolver, Subscriber, ObservableSubscriber, AsyncIteratorSubscriber, } from '@cortical/ts2graphql';
export * from './api/context';
export interface ExecutionResultDataDefault {
    [key: string]: any;
}
export interface InputExecutionResult<TData = ExecutionResultDataDefault> {
    errors?: ReadonlyArray<InputError>;
    data?: TData;
}
export declare type InputError = Error | GraphQLError | ApolloError;
export declare function formatErrors<T extends BaseContext = BaseContext>(context: T | undefined, errors: ReadonlyArray<InputError>, options?: {
    formatter?: Function;
    debug?: boolean;
}): ReadonlyArray<ApolloError>;
export declare function formatResponse<T extends BaseContext = BaseContext>(context: T | undefined, response: InputExecutionResult, options?: {
    formatter?: Function;
    debug?: boolean;
}, end?: boolean): ExecutionResult;
export declare function executeWithContext<T extends ExecutionResultDataDefault>(schema: GraphQLSchema, document: DocumentNode, rootValue: any, context: BaseContext, variableValues?: {
    [key: string]: any;
}, operationName?: string, fieldResolver?: GraphQLFieldResolver<any, any>): Promise<ExecutionResult<T>>;
export declare function execute<T extends ExecutionResultDataDefault>(document: DocumentNode, url: UrlWithParsedQuery, headers: IncomingHttpHeaders, params: ContextParams, variableValues?: {
    [key: string]: any;
}, operationName?: string, fieldResolver?: GraphQLFieldResolver<any, any>): Promise<ExecutionResult>;
export declare function subscribeWithContext<T extends ExecutionResultDataDefault>(schema: GraphQLSchema, document: DocumentNode, rootValue: any, context: BaseContext, variableValues?: {
    [key: string]: any;
}, operationName?: string): Promise<ExecutionResult<T> | AsyncIterator<ExecutionResult<T>>>;
export declare function subscribe<T extends ExecutionResultDataDefault>(document: DocumentNode, url: UrlWithParsedQuery, headers: IncomingHttpHeaders, params: ContextParams, variableValues?: {
    [key: string]: any;
}, operationName?: string): Promise<ExecutionResult | AsyncIterator<ExecutionResult>>;
