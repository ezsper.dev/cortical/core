import { ApolloError } from 'apollo-server-errors';
export declare class EntityNotFoundError extends ApolloError {
    constructor(entityName: string, criteria: any);
}
