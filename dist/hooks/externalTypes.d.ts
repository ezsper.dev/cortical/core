import { HookFilterSync } from '@spine/hook';
import { GraphQLNamedType } from 'graphql';
export interface Params {
}
declare const _default: HookFilterSync<GraphQLNamedType[], Params>;
export default _default;
