import { HookAction } from '@spine/hook';
import { Request } from 'express';
export interface Params {
    req: Request;
}
declare const _default: HookAction<void, Params>;
export default _default;
