import { HookActionSync } from '@spine/hook';
import { GraphQLSchema } from 'graphql';
export interface Params {
    schema: GraphQLSchema;
}
declare const _default: HookActionSync<void, Params>;
export default _default;
