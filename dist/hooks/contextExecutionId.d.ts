import { HookFilterSync } from '@spine/hook';
import { GraphQLResolveInfo } from 'graphql';
import { BaseContext } from '../api/context';
export interface Params {
    context: BaseContext;
    info?: GraphQLResolveInfo;
}
declare const _default: HookFilterSync<any, Params>;
export default _default;
