import { HookFilterSync } from '@spine/hook';
import { ContextParams } from '../api/context';
import * as WebSocket from 'ws';
import { OperationMessage, ExecutionParams } from 'subscriptions-transport-ws';
export interface Params {
    webSocket: WebSocket;
    params: ExecutionParams;
    message: OperationMessage;
}
declare const _default: HookFilterSync<ContextParams, Params>;
export default _default;
