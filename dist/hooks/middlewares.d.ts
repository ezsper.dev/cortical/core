import { HookActionSeries } from '@spine/hook';
import { Application } from '../server/Application';
export interface Params {
    api: Application;
}
declare const _default: HookActionSeries<void, Params>;
export default _default;
