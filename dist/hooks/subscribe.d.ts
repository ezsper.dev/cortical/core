import { HookFilter } from '@spine/hook';
import { DocumentNode, GraphQLSchema } from 'graphql';
import { ExecutionResult } from '..';
import { BaseContext } from '../api/context';
export interface Params {
    schema: GraphQLSchema;
    document: DocumentNode;
    rootValue: any;
    context: BaseContext;
    variableValues?: {
        [key: string]: any;
    };
    operationName?: string;
}
declare const _default: HookFilter<void | ExecutionResult<import("graphql/execution/execute").ExecutionResultDataDefault> | AsyncIterator<ExecutionResult<import("graphql/execution/execute").ExecutionResultDataDefault>>, Params>;
export default _default;
