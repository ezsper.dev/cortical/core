import {
  ConfigSource as BaseConfigSource,
  config as baseConfig,
} from '@cortical/core/config';

export interface MyConfigSource {
  // put your custom config here
  // facebookSecret: string;
  // nowApublicConfig: string;
  // remove the comment below and be open about your config
  // [key: string]: any;
}

export interface ConfigSource extends BaseConfigSource, MyConfigSource {}
export const config = baseConfig.extend<ConfigSource>();

require('./load');