import { HookFilterSync } from '@spine/hook';
import { GraphQLSchema } from 'graphql';
import { IResolvers } from './resolvers';
export interface Params {
    resolvers: IResolvers;
}
declare const _default: HookFilterSync<GraphQLSchema, Params>;
export default _default;
