import { default as bootstrapHook } from '@spine/bootstrap/hooks/bootstrap';
export * from '@spine/bootstrap/hooks/bootstrap';
export default bootstrapHook;
