import { HookFilterSync } from '@spine/hook';
import { BaseContext } from '../api/context';
export interface Params {
    context: BaseContext;
}
declare const _default: HookFilterSync<any, Params>;
export default _default;
