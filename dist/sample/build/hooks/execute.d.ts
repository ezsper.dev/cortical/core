import { HookFilter } from '@spine/hook';
import { BaseContext } from '../api/context';
import { DocumentNode, GraphQLSchema, GraphQLFieldResolver } from 'graphql';
import { ExecutionResult } from '..';
export interface Params {
    schema: GraphQLSchema;
    rootValue: any;
    context: BaseContext;
    document: DocumentNode;
    variableValues?: {
        [key: string]: any;
    };
    operationName?: string;
    fieldResolver?: GraphQLFieldResolver<any, any> | null;
}
declare const _default: HookFilter<void | ExecutionResult<import("graphql/execution/execute").ExecutionResultDataDefault>, Params>;
export default _default;
