import { HookFilterSync } from '@spine/hook';
import { IResolvers } from 'graphql-tools/dist/Interfaces';
export { IResolvers };
export interface Params {
}
declare const _default: HookFilterSync<IResolvers<any, any>, Params>;
export default _default;
