import { ApolloError } from 'apollo-server-errors';
import { HookFilterSync } from '@spine/hook';
import { BaseContext } from '../api/context';
export interface Params {
    context?: BaseContext;
    originalError?: Error;
    internalError?: Error;
}
declare const _default: HookFilterSync<ApolloError, Params>;
export default _default;
