export declare function siteUrl(path?: string): string;
export declare function basePath(path?: string): string;
export declare function baseWSPath(path?: string): string;
