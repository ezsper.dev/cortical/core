"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const resolvers_1 = require("../hooks/resolvers");
const getTranspiler_1 = require("./getTranspiler");
const context_1 = require("../api/context");
const iterall_1 = require("iterall");
let resolvers;
function mapContextExecution(resolvers) {
    const newResolvers = {};
    const mapResolver = (resolver) => ((rootValue, args, context, info) => __awaiter(this, void 0, void 0, function* () {
        const execution = yield context_1.createExecutionContext(context, info);
        try {
            const result = yield resolver(rootValue, args, execution, info);
            if (iterall_1.isAsyncIterable(result)) {
                return (function () {
                    return __asyncGenerator(this, arguments, function* () {
                        var e_1, _a;
                        try {
                            try {
                                for (var result_1 = __asyncValues(result), result_1_1; result_1_1 = yield __await(result_1.next()), !result_1_1.done;) {
                                    const data = result_1_1.value;
                                    yield yield __await(data);
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (result_1_1 && !result_1_1.done && (_a = result_1.return)) yield __await(_a.call(result_1));
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                        }
                        catch (error) {
                            if (!execution.completed) {
                                execution.end(error);
                            }
                            throw error;
                        }
                    });
                })();
            }
            return result;
        }
        catch (error) {
            if (!execution.completed) {
                execution.end(error);
            }
            throw error;
        }
    }));
    for (const key in resolvers) {
        const resolver = resolvers[key];
        if (typeof resolver === 'object' && resolver != null) {
            newResolvers[key] = {};
            for (const field in resolver) {
                const fieldResolver = resolver[field];
                if (typeof fieldResolver === 'function') {
                    newResolvers[key][field] = mapResolver(fieldResolver);
                }
                else {
                    newResolvers[key][field] = Object.assign({}, fieldResolver);
                    if (typeof fieldResolver.subscribe === 'function') {
                        newResolvers[key][field].subscribe = mapResolver(fieldResolver.subscribe);
                    }
                    if (typeof fieldResolver.resolve === 'function') {
                        newResolvers[key][field].resolve = mapResolver(fieldResolver.resolve);
                    }
                }
            }
        }
    }
    return newResolvers;
}
resolvers_1.default.addFilter('executionContext', mapContextExecution);
function getResolvers(schema) {
    if (resolvers !== undefined) {
        return resolvers;
    }
    const transpiler = getTranspiler_1.getTranspiler();
    resolvers = transpiler.getResolvers(schema);
    resolvers = resolvers_1.default.filter(resolvers, {});
    return resolvers;
}
exports.getResolvers = getResolvers;
//# sourceMappingURL=getResolvers.js.map