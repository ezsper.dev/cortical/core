"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const schema_1 = require("../hooks/schema");
const schemaReady_1 = require("../hooks/schemaReady");
const getTranspiler_1 = require("./getTranspiler");
const paths_1 = require("../paths");
const getResolvers_1 = require("./getResolvers");
const logger_1 = require("@spine/logger");
exports.log = logger_1.logger('cortical:schema');
let schema;
function getSchema() {
    if (schema !== undefined) {
        return schema;
    }
    const transpiler = getTranspiler_1.getTranspiler();
    switch (process.env.CORTICAL_COMMAND) {
        case 'develop':
        case 'build':
            // We transpile GraphQL at runtime(ish)
            exports.log.info('Generating...');
            transpiler.generate((result) => {
                if (result.stdout != null) {
                    result.stdout.toString().split(/\n/).forEach((data) => {
                        const message = data.trim();
                        if (message.length > 0) {
                            console.log(message);
                        }
                    });
                }
            }, ['ignore', 'pipe', 'inherit']);
            schema = transpiler.getSchema();
            break;
        default:
            if (!fs.existsSync(paths_1.getBuildSchemaPath())) {
                throw new Error('No schema build was found');
            }
            // In production we use the generated schema files
            exports.log.info('Loading...');
            schema = transpiler.restoreSchema(paths_1.getBuildSchemaPath());
    }
    const resolvers = getResolvers_1.getResolvers(schema);
    schema = schema_1.default.filter(schema, { resolvers });
    transpiler.addResolveFunctionsToSchema(schema, resolvers);
    /*
    if (!config.server.ws.disable) {
      delete (<any>schema)._subscriptionType;
    }
    */
    schemaReady_1.default.do({ schema });
    return schema;
}
exports.getSchema = getSchema;
//# sourceMappingURL=getSchema.js.map