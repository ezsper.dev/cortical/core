"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@cortical/core");
require("@cortical/types/bootstrap");
// add your hooks and plugins in between
core_1.bootstrap()
    .catch(error => {
    console.log(error.stack !== undefined ? error.stack : error.message);
});
//# sourceMappingURL=index.js.map