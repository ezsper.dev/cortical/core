"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Package_1 = require("../types/Package");
exports.resolvePackage = (args, context) => {
    const packageJson = require('../../package.json');
    const { name, version, description, dependencies, } = packageJson;
    return new Package_1.Package({
        name,
        version,
        description,
        dependencies,
    });
};
//# sourceMappingURL=package.js.map