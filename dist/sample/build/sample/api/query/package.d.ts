import { Resolver } from '@cortical/core';
import { Package } from '../types/Package';
import { Context } from '../context';
export declare const resolvePackage: Resolver<Package, void, Context>;
