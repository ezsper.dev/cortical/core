"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@cortical/core");
class Package {
    constructor(obj) {
        Object.assign(this, obj);
    }
    getDependencies() {
        return Object.keys(this.dependencies);
    }
    getDependencyVersion(args) {
        if (this.dependencies.hasOwnProperty(args.name)) {
            return this.dependencies[args.name];
        }
        throw new core_1.GraphQLError(`Couldn't found dependency ${args.name}`);
    }
}
exports.Package = Package;
//# sourceMappingURL=Package.js.map