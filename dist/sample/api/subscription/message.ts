import { Subscriber } from '@cortical/core';
import { Context } from '../context';

export const subscribeMessage: Subscriber<string, void, Context> = async function* (args, context) {
  context.end(() => {
    context.debug('completed');
  });
  while (!context.completed) {
    yield `Hello from WebSocket ${Math.random()}`;
    await new Promise(resolve => setTimeout(resolve, 1000));
  }
};