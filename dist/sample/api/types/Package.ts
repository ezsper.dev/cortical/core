import { GraphQLError } from '@cortical/core';

export type Fields =
  'name'
  | 'version'
  | 'description'
  | 'dependencies';

export class Package {

  public name: string;
  public version: string;
  public description: string;
  public dependencies: any;

  constructor(obj: Pick<Package, Fields>) {
    Object.assign(this, obj);
  }

  public getDependencies(): string[] {
    return Object.keys(this.dependencies);
  }

  public getDependencyVersion(
    args: {
      name: string,
    },
  ): string {
    if (this.dependencies.hasOwnProperty(args.name)) {
      return this.dependencies[args.name];
    }
    throw new GraphQLError(`Couldn't found dependency ${args.name}`);
  }

}
