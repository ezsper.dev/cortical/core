import { Resolver } from '@cortical/core';
import { Context } from '../context';
import { resolvePackage } from './package';

export const packageName: Resolver<string, void, Context> = async (args, context) => {
  return (await resolvePackage(undefined, context)).name;
};