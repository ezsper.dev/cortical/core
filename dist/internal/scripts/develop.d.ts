/**
 * This script builds a production output of all of our bundles.
 */
import { Arguments } from './build.yargs';
declare const _default: (argv: Arguments) => Promise<void>;
export default _default;
