import { Arguments } from './start.yargs';
declare const _default: (argv: Arguments) => Promise<void>;
export default _default;
