"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This script builds a production output of all of our bundles.
 */
const path_1 = require("path");
const create_yargs_1 = require("./create.yargs");
const Prompt_1 = require("../utils/Prompt");
const fs = require("fs-extra");
exports.default = (argv) => __awaiter(this, void 0, void 0, function* () {
    const prompt = new Prompt_1.Prompt();
    let name = argv.name;
    let license = 'MIT';
    let answer;
    answer = yield prompt.ask(`Give the name of your project [${name}]:\n`);
    if (answer !== undefined && answer.trim() !== '') {
        name = answer.trim();
    }
    answer = yield prompt.ask(`Give the license of your project [${license}]:\n`);
    if (answer !== undefined && answer.trim() !== '') {
        license = answer.trim();
    }
    prompt.close();
    const projectPath = path_1.resolve(process.cwd(), argv.name);
    if (fs.existsSync(projectPath)) {
        console.error(`Directory ${argv.name} already exists`);
        process.exit(0);
    }
    fs.copySync(path_1.resolve(__dirname, '..', '..', 'sample'), projectPath);
    const packageJsonPath = path_1.resolve(projectPath, 'package.json');
    const packageJson = JSON.parse(fs.readFileSync(packageJsonPath).toString('utf8'));
    packageJson.name = name;
    packageJson.license = license;
    fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2));
});
if (!module.parent) {
    create_yargs_1.handler(create_yargs_1.builder().argv)
        .catch(err => {
        console.log(err.stack);
    });
}
//# sourceMappingURL=create.js.map