"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This script builds to distribuition
 */
const appRootDir = require("app-root-dir");
const path_1 = require("path");
const fs = require("fs-extra");
const exec_1 = require("../utils/exec");
function fileFilter(target, filter, encoding = 'utf8') {
    fs.writeFileSync(target, filter(fs.readFileSync(target).toString(encoding)), encoding);
}
// First clear the build output dir.
exec_1.exec(`rimraf ${path_1.resolve(appRootDir.get(), 'dist')}`);
// Build typescript
exec_1.exec('tsc --outDir dist');
// Copy documents
fs.copySync(path_1.resolve(appRootDir.get(), 'README.md'), path_1.resolve(appRootDir.get(), 'dist/README.md'));
// Copy bin
fs.copySync(path_1.resolve(appRootDir.get(), 'bin'), path_1.resolve(appRootDir.get(), 'dist/bin'));
// replace ts-node for node
fileFilter(path_1.resolve(appRootDir.get(), 'dist/bin/cortical'), data => data.replace(/.*ts-node\/register.*/, ''));
// Clear our package json for release
const packageJson = require('../../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson['lint-staged'];
delete packageJson.jest;
const { devDependencies } = packageJson;
delete packageJson.devDependencies;
fs.writeFileSync(path_1.resolve(appRootDir.get(), './dist/package.json'), JSON.stringify(packageJson, null, 2));
// Copy .gitignore
fs.copySync(path_1.resolve(appRootDir.get(), '.gitignore'), path_1.resolve(appRootDir.get(), 'dist/.gitignore'));
fs.removeSync(path_1.resolve(appRootDir.get(), 'dist/sample'));
fs.copySync(path_1.resolve(appRootDir.get(), 'sample'), path_1.resolve(appRootDir.get(), 'dist/sample'));
fs.copySync(path_1.resolve(appRootDir.get(), 'cortical.png'), path_1.resolve(appRootDir.get(), 'dist/cortical.png'));
const samplePackageJson = require('../../dist/sample/package.json');
samplePackageJson.dependencies['@cortical/core'] = `^${packageJson.version}`;
samplePackageJson.dependencies['@cortical/types'] = `${devDependencies['@cortical/types']}`;
samplePackageJson.dependencies['typescript'] = `${devDependencies['typescript']}`;
fs.writeFileSync(path_1.resolve(appRootDir.get(), 'dist/sample/package.json'), JSON.stringify(samplePackageJson, null, 2));
//# sourceMappingURL=release.js.map