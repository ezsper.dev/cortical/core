"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const start_yargs_1 = require("./start.yargs");
const build_1 = require("./build");
const paths_1 = require("../../paths");
const child_process_1 = require("child_process");
const notify_1 = require("../utils/notify");
exports.default = (argv) => __awaiter(this, void 0, void 0, function* () {
    if (argv.rebuild || !fs.existsSync(paths_1.getBuildPath())) {
        yield build_1.default({ _: [], $0: '' });
    }
    const child = child_process_1.spawn('node', [
        '-r',
        'tsconfig-paths/register',
        '-r',
        '@cortical/core',
        paths_1.getBuildRootPath(),
        '--color',
    ], {
        cwd: paths_1.getBuildRootPath(),
        env: Object.assign({}, process.env, { PATH: process.env.PATH, NODE_ENV: 'production' }, (process.env.CORTICAL_DIR != null && {
            CORTICAL_DIR: process.env.CORTICAL_DIR,
        }), { CORTICAL_COMMAND: 'start' }),
    });
    child.stdout.on('data', data => console.log(data.toString().trim()));
    child.stderr.on('data', (data) => {
        console.error(data.toString().trim());
    });
    child.once('exit', (code, signal) => {
        if (code > 0) {
            notify_1.notify('✖️ An error has occured, check the terminal');
        }
        else {
            notify_1.notify('✖️ Server was shutdown abruptly');
        }
    });
});
if (!module.parent) {
    start_yargs_1.handler(start_yargs_1.builder().argv)
        .catch(err => {
        console.log(err.stack);
    });
}
//# sourceMappingURL=start.js.map