import * as notifier from 'node-notifier';
export declare function notify(message: string, options?: notifier.Notification): notifier.NodeNotifier;
