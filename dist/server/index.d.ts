/// <reference types="node" />
import * as http from 'http';
import { Application } from './Application';
export { Application, Request, Response } from './Application';
export { NextFunction } from 'express';
export declare const api: Application<import("@cortical/core/api/context").BaseContext>;
export declare const log: import("@spine/logger").Log<{
    error: number;
    warn: number;
    special: number;
    info: number;
    verbose: number;
    debug: number;
    silly: number;
}, void | {
    readonly name: string;
    readonly options: import("@spine/logger").LoggerOptions<any>;
    getDisplayName(): string;
    enabled(label: any): boolean;
    child<C extends import("@spine/logger").LogLevels = any>(name: string, options?: import("@spine/logger").LoggerOptions<C> | undefined): import("@spine/logger").Log<C, import("@spine/logger").LoggerMember<any, any>>;
    log: import("@spine/logger").Log<any, any>;
} | ({
    readonly name: string;
    readonly options: import("@spine/logger").LoggerOptions<any>;
    getDisplayName(): string;
    enabled(label: any): boolean;
    child<C extends import("@spine/logger").LogLevels = any>(name: string, options?: import("@spine/logger").LoggerOptions<C> | undefined): import("@spine/logger").Log<C, import("@spine/logger").LoggerMember<any, any>>;
    log: import("@spine/logger").Log<any, any>;
} & {
    readonly parent: any;
})>;
export declare const httpServer: http.Server;
export default api;
