import * as Express from 'express';
import * as http from 'http';
import { json } from 'body-parser';
import * as cors from 'cors';
import * as accepts from 'accepts';
import {
  renderPlaygroundPage,
  RenderPageOptions as PlaygroundRenderPageOptions,
} from '@apollographql/graphql-playground-html';
import { ApolloServer } from 'apollo-server-express';
import {
  SubscriptionServer,
  ConnectionContext,
  OperationMessage,
  ExecutionParams,
} from 'subscriptions-transport-ws';
import * as WebSocket from 'ws';
import { getSchema } from '../api/getSchema';
import { siteUrl, basePath, baseWSPath } from '../api/url';
import { default as deathHook } from '../hooks/death';
import { default as initHook } from '../hooks/init';
import { default as middlewaresHook } from '../hooks/middlewares';
import { default as listeningHook } from '../hooks/listening';
import { default as gatewayRequestHook } from '../hooks/gatewayRequest';
import { default as gatewayConnectHook } from '../hooks/gatewayConnect';
import { default as serverLaunchInfoHook } from '../hooks/serverLaunchInfo';
import { Application, Request, Response } from './Application';
import { logger, LogOptions, LogExec } from '@spine/logger';
import { config } from '../config';
import {
  executeWithContext,
  subscribeWithContext,
  formatErrors,
  formatResponse,
  InputError,
} from '..';
import {
  createContextFromExpressRequest,
  createContextFromWebSocketTransport,
} from '../api/context';

export { Application, Request, Response } from './Application';
export { NextFunction } from 'express';

// Create our express based server.

export const api = <Application>Object.assign(
  Express(),
  {},
);

const methods = [
  'use',
  'all',
  'get',
  'post',
  'put',
  'delete',
  'patch',
  'options',
  'head',
  'checkout',
  'connect',
  'copy',
  'lock',
  'merge',
  'mkactivity',
  'mkcol',
  'move',
  'm-search',
  'notify',
  'propfind',
  'proppatch',
  'purge',
  'report',
  'search',
  'subscribe',
  'trace',
  'unlock',
  'unsubscribe',
];

function applyReq(req: Express.Request) {
  if (!('context' in req)) {
    Object.defineProperty(req, 'context', {
      enumerable: true,
      configurable: true,
      get() {
        throw new Error(`Context is not ready`);
      },
    });
  }
  return <any>req;
}

function applyRes(res: Express.Response) {
  if (typeof (<any>res).sendResult === 'undefined') {
    Object.defineProperty(res, 'sendResult', {
      writable: true,
      configurable: true,
      value: function sendResult(result: any) {
        res.json(formatResponse((<any>res).req.context, result));
        return res;
      },
    });
  }
  if (typeof (<any>res).sendData === 'undefined') {
    Object.defineProperty(res, 'sendData', {
      writable: true,
      configurable: true,
      value: function sendData(data: any) {
        (<any>res).sendResult({ data });
        return res;
      },
    });
  }
  if (typeof (<any>res).sendErrors === 'undefined') {
    Object.defineProperty(res, 'sendErrors', {
      writable: true,
      configurable: true,
      value: function sendErrors(errors: any) {
        (<any>res).sendResult({ errors });
        return res;
      },
    });
  }
  return <any>res;
}

function applyNext(req: any, res: any, next: Express.NextFunction) {
  return (error?: any) => {
    if (error == null) {
      return next();
    }
    handleExpressError(error, req, res, next);
  };
}

function handleExpressError(error: Error, req: Request, res: Response, next: Express.NextFunction) {
  res.json(formatResponse(contextReady ? req.context : undefined, {errors: [error], data: undefined}));
}

function mapHandler(handler: any) {
  if (typeof handler === 'function') {
    const length = handler.length;
    const newHandler = async (...args: any[]) => {
      let parsedArgs: any[];
      if (args[0] instanceof Error) {
        const [error, req, res, next] = args;
        const newReq = applyReq(req);
        const newRes = applyRes(res);
        const newNext = applyNext(newReq, newRes, next);
        parsedArgs = [error, newReq, newRes, newNext];
      } else {
        const [req, res, next] = args;
        const newReq = applyReq(req);
        const newRes = applyRes(res);
        const newNext = applyNext(newReq, newRes, next);
        parsedArgs = [newReq, newRes, newNext];
      }
      try {
        return await handler(...parsedArgs);
      } catch (error) {
        const next = parsedArgs[args.length - 1];
        return await next(error);
      }
    };
    Object.defineProperty(newHandler, 'length', {
      enumerable: false,
      writable: false,
      configurable: true,
      value: length,
    });
    return newHandler;
  }
  return handler;
}

methods.forEach(method => {
  const originalMethod = (<any>api)[method];
  Object.assign(api, {
    // tslint:disable-next-line function-name
    [method](...args: any[]) {
      const newArgs = args.slice(0);
      for (const index in newArgs) {
        const handler = newArgs[index];
        if (Array.isArray(handler)) {
          for (const subindex in handler) {
            handler[subindex] = mapHandler(handler[subindex]);
          }
        } else {
          newArgs[index] = mapHandler(handler);
        }
      }
      return originalMethod.apply(this, newArgs);
    },
  });
});

export const log = logger('server');

export const httpServer = http.createServer(api);

middlewaresHook.addAction('body.json', async ({ api }) => {
  api.use(config.server.path, json(<any>config.server.json));
}, { priority: 5 });

middlewaresHook.addAction('cors', async ({ api }) => {
  api.use(config.server.path, cors(<any>config.server.cors));
}, { priority: 5 });

let contextReady = false;
middlewaresHook.addAction('context', () => {
  api.use(config.server.path, async (req, res, next) => {
    await gatewayRequestHook.do({ req });
    const context = await createContextFromExpressRequest(req);
    Object.defineProperty(req, 'context', {
      enumerable: true,
      writable: false,
      configurable: true,
      value: context,
    });
    contextReady = true;
    next();
  });
}, { priority: 8 });

middlewaresHook.addAction('errors', async ({ api }) => {
  api.use(config.server.path, handleExpressError);
}, { priority: 1 });

middlewaresHook.addAction('graphql', async ({ api }) => {
  api.post(basePath(config.server.graphqlPath), async (req, res, next) => {
    try {
      const context = req.context;
      const apolloServer = new ApolloServer({
        context,
        schema: context.schema,
        debug: false,
        formatResponse(response: any, options: any) {
          return formatResponse(context, response);
        },
        formatError: (error: any) => {
          return formatErrors(context, Array.isArray(error) ? error : [error]);
        },
      });
      require('apollo-server-express/dist/expressApollo').graphqlExpress(
        () => apolloServer.createGraphQLServerOptions(req, res),
      )(req, res, next);
    } catch (error) {
      next(error);
    }
  });
}, { priority: 50 });

const {
  disable: disablePlayground,
  path: playgroundPath,
  ...playgroundOptions
} = config.server.playground;

const playgroundHook = middlewaresHook.addAction('playground', async ({ api }) => {
  if (process.env.NODE_ENV === 'development') {
    const reload: any = require('reload')(api);
    deathHook.addAction('reload', () => {
      if (typeof reload.wss !== 'undefined') {
        reload.wss.close();
      }
    });
  }
  api.get(basePath(playgroundPath), (req, res, next) => {
    if (process.env.NODE_ENV === 'development') {
      const reloadInjection = '<script src="/reload/reload.js"></script>';
      const write = res.write.bind(res);
      Object.assign(res, {
        write(body: any) {
          return write(
            body.replace(/(<\/body>)/, `  ${reloadInjection}\n$1`),
          );
        }
      });
    }
    // perform more expensive content-type check only if necessary
    // XXX We could potentially move this logic into the GuiOptions lambda,
    // but I don't think it needs any overriding
    const accept = accepts(req);
    const types = accept.types() as string[];
    const prefersHTML =
      types.find(
        (x: string) => x === 'text/html' || x === 'application/json',
      ) === 'text/html';

    if (prefersHTML) {
      const playgroundRenderPageOptions: PlaygroundRenderPageOptions = <any>{
        endpoint: basePath(config.server.graphqlPath),
        ...(!config.server.ws.disable && {
          subscriptionEndpoint: baseWSPath(config.server.ws.graphqlPath),
        }),
        ...playgroundOptions,
      };
      res.setHeader('Content-Type', 'text/html');
      const playground = renderPlaygroundPage(playgroundRenderPageOptions);
      res.write(playground);
      res.end();
      return;
    }
  });
}, { priority: 60 });

if (disablePlayground) {
  playgroundHook.disable();
}

initHook.addAction('middlewares', async () => {
  await middlewaresHook.do({ api });
});


initHook.addAction('serverStartingLog', async () => {
  if (process.env.SERVER_RESTART !== 'true') {
    log.verbose('Launching...');
  }
}, { before: 'server' });

serverLaunchInfoHook.addFilter('websocket', (items) => {
  return [
    ...items,
    process.stdout.isTTY
      ? `WebSocket/Subscriptions: ${config.server.ws.disable ? 'Disabled' : 'Enabled'}`
      : `🔴 WebSocket/Subscriptions: ${config.server.ws.disable ? '❌ Disabled' : '✅ Enabled'}`
    ,
  ];
});

listeningHook.addAction('log', async () => {
  if (process.env.SERVER_RESTART === 'true') {
    log.info(`Up to date!`);
  } else {
    if (process.stdout.isTTY) {
      log.info(`Listening on Port ${config.server.listenPort} and Address ${config.server.listenAddress}`);
      log.info(`Available in the browser at ${siteUrl()}`);
      log.info(serverLaunchInfoHook.filter([], {}).join('\n      '));
    } else {
      console.log(`

      You are all set!

      Now listening on Port ${config.server.listenPort} and Address ${config.server.listenAddress}
      Available in the browser at ${siteUrl()}
      
      ${serverLaunchInfoHook.filter([], {}).join('\n      ')}
      
      Press Ctrl-C to stop.



    `);
    }
  }
  if (process.env.NODE_ENV === 'development' && process.env.SERVER_RESTART === 'true') {
    const { notify } = await import('../internal/utils/notify');
    notify('✔️ Server is running with latest changes');
  }
});

initHook.addAction('subscription', () => {
  const subscribeServer = new SubscriptionServer(
    {
      execute: executeWithContext,
      subscribe: subscribeWithContext,
      schema: getSchema(),
      async onConnect(connectionParams: Object, webSocket: WebSocket, context: ConnectionContext) {
        const operationContext: any = {};
        Object.assign(<any>webSocket, { operationContext });
        await gatewayConnectHook.do({ connectionParams, webSocket, context });
        return true;
      },
      async onOperation(message: OperationMessage, params: ExecutionParams, webSocket: WebSocket) {
        const { id: opId } = message;
        if (opId == null) {
          throw new Error('No operation id set');
        }
        const context = await createContextFromWebSocketTransport(webSocket, message, params);
        (<any>webSocket).operationContext[opId] = context;
        return {
          ...params,
          context,
        };
      },
      async onOperationComplete(webSocket: WebSocket, opId: string) {
        const { operationContext } = (<any>webSocket);
        if (typeof operationContext === 'undefined') {
          return;
        }
        const context = operationContext[opId];
        if (context != null && !context.completed) {
          context.end(null, 'unsubscribed');
        }
      },
    },
    {
      server: httpServer,
      path: basePath(config.server.ws.graphqlPath),
    },
  );
  deathHook.addAction('subscription', () => {
    subscribeServer.close();
  }, 100);
});

initHook.addAction('server', async () => {
  await new Promise((resolve, reject) => {
    // Create an http listener for our express app.
    httpServer.listen(config.server.listenPort, config.server.listenAddress, () => {
      // Execute listening hook
      listeningHook.do({ httpServer, api })
        .then(resolve)
        .catch(reject);
    });

    httpServer.on('error', reject);
  });
  deathHook.addAction('server', async () => {
    await new Promise((resolve, reject) => {
      httpServer.close((error: Error) => {
        if (error !== undefined) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  });
}, { priority: 50 });

export default api;
