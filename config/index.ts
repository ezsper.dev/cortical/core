import {
  BaseConfigSource,
  createConfig,
} from '@spine/config';

import { OptionsJson } from 'body-parser';
import { CorsOptions } from 'cors';
import { RenderPageOptions as PlaygroundRenderPageOptions } from '@apollographql/graphql-playground-html';

export { BaseConfigSource };

export interface CorticalConfigSource {
  server: {
    /**
     * The address the server will listen
     */
    listenAddress: string;
    /**
     * The port the server will listen
     */
    listenPort: number;
    /**
     * The host accessable through the browser
     */
    host: string;
    /**
     * JSON body parser options
     */
    json: OptionsJson;
    /**
     * CORS options
     */
    cors: CorsOptions;
    /**
     * GraphQL HTTP path
     */
    path: string;
    graphqlPath: string;
    ws: {
      /**
       * Disable websocket
       */
      disable: boolean;
      /**
       * GrqphQL Websocket path
       */
      path: string;
      graphqlPath: string;
    },
    /**
     * Apollo Playground options
     */
    playground: PlaygroundRenderPageOptions & {
      /**
       * Disables playground
       */
      disable: boolean;
      /**
       * Playground path
       */
      path: string;
    };
  },
}

export interface ConfigSource extends BaseConfigSource, CorticalConfigSource {}
export const config = createConfig<ConfigSource>('Cortical');