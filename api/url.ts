import * as url from 'url';
import { config } from '../config/index';

export function siteUrl(path: string = '') {
  return `http://${config.server.host}${basePath(path)}`;
}

export function basePath(path: string = '') {
  return url.resolve(config.server.path, path.replace(/^\//, ''));
}

export function baseWSPath(path: string = '') {
  return basePath(url.resolve(config.server.ws.path, path.replace(/^\//, '')));
}