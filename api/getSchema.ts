import { GlobSync } from 'glob';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs';
import {
  GraphQLSchema,
  GraphQLScalarType,
  GraphQLObjectType,
  GraphQLEnumType,
} from 'graphql';
import { addResolveFunctionsToSchema } from 'graphql-tools';
import { IResolvers } from 'graphql-tools/dist/Interfaces';
import { default as schemaHook } from '../hooks/schema';
import { default as schemaReadyHook } from '../hooks/schemaReady';
import { values } from 'lodash';
import { getTranspiler } from './getTranspiler';
import { getBuildSchemaPath } from '../paths';
import { getResolvers } from './getResolvers';
import { config } from '../config';
import { logger, LogOptions, LogExec } from '@spine/logger';

export const log = logger('cortical:schema');

let schema: GraphQLSchema;
export function getSchema(): GraphQLSchema {
  if (schema !== undefined) {
    return schema;
  }
  const transpiler = getTranspiler();
  switch (process.env.CORTICAL_COMMAND) {
    case 'develop':
    case 'build':
      // We transpile GraphQL at runtime(ish)
      log.info('Generating...');
      transpiler.generate((result) => {
        if (result.stdout != null) {
          result.stdout.toString().split(/\n/).forEach((data) => {
            const message = data.trim();
            if (message.length > 0) {
              console.log(message);
            }
          });
        }
      }, ['ignore', 'pipe', 'inherit']);
      schema = transpiler.getSchema();
      break;
    default:
      if (!fs.existsSync(getBuildSchemaPath())) {
        throw new Error('No schema build was found');
      }
      // In production we use the generated schema files
      log.info('Loading...');
      schema = transpiler.restoreSchema(getBuildSchemaPath());
  }
  const resolvers = getResolvers(schema);
  schema = schemaHook.filter(schema, { resolvers });
  transpiler.addResolveFunctionsToSchema(schema, resolvers);
  /*
  if (!config.server.ws.disable) {
    delete (<any>schema)._subscriptionType;
  }
  */
  schemaReadyHook.do({ schema });
  return schema;
}
