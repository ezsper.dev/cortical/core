import { EventEmitter } from 'events';
import {
  resolve as pathResolve,
  relative as pathRelative,
  basename,
} from 'path';
import {
  hooks,
  InputError,
  ExecutionResultDataDefault,
} from '..';
import * as http from 'http';
import {parse as urlParse, UrlWithParsedQuery} from 'url';
import { logger, Log, LogLevels, LogOptions, LogExec } from '@spine/logger';
import { HookListener } from '@spine/hook';
import { getApiPath, getRootPath } from '../paths';
import { Request } from 'express';
import { default as expressRequestContextParamsHook } from '../hooks/expressRequestContextParams';
import { default as webSocketTransportContextParamsHook } from '../hooks/webSocketTransportContextParams';
import { default as contextHook, Params as ContextHookParams } from '../hooks/context';
import { default as contextExecutionHook } from '../hooks/contextExecution';
import * as WebSocket from 'ws';
import * as fs from 'fs';
import {
  OperationMessage,
  ExecutionParams,
} from 'subscriptions-transport-ws';
import {
  execute as corticalExecute,
  subscribe as corticalSubscribe,
} from '..';
import { basePath, baseWSPath } from './url';
import { config } from '../config';
import {
  GraphQLSchema,
  GraphQLResolveInfo,
  DocumentNode,
  ExecutionResult,
} from 'graphql';
import { getSchema } from '../api/getSchema';
import { default as contextIdHook } from '../hooks/contextId';
import { default as contextExecutionIdHook } from '../hooks/contextExecutionId';

export interface ContextExecute {
  <T extends ExecutionResultDataDefault>(
    document: DocumentNode,
    variableValues?: {[key: string]: any},
    operationName?: string,
  ): Promise<ExecutionResult<T>>
}

export interface ContextSubscribe {
  <T extends ExecutionResultDataDefault>(
    document: DocumentNode,
    variableValues?: {[key: string]: any},
    operationName?: string
  ): Promise<ExecutionResult<T> | AsyncIterator<ExecutionResult<T>>>
}

export interface ContextParams {
  [key: string]: any;
}

contextIdHook.addFilter('default', () => {
  return `${(new Date()).getTime()}.${Math.floor(Math.random() * Math.pow(10, 5))}`;
});
const $$executions = Symbol();
const $$executionSeq = Symbol();
const $$end = Symbol();
contextExecutionIdHook.addFilter('default', (value, { context }) => {
  if (typeof (<any>context)[$$executionSeq] === 'undefined') {
    (<any>context)[$$executionSeq] = -1;
  }
  (<any>context)[$$executionSeq] = (<any>context)[$$executionSeq] + 1;
  return `${context.id}.${(<any>context)[$$executionSeq]}`;
});

export function createContextId(context: BaseContext) {
  return contextIdHook.filter(null, { context });
}

export function createExecutionContextId(context: BaseContext, info?: GraphQLResolveInfo) {
  return contextExecutionIdHook.filter(null, { context, info });
}


const connections = new Set<BaseContext>();
hooks.bootstrap.addAction('connections', async () => {
  const closeConnections = () => {
    while (connections.size > 0) {
      for (const connection of connections.values()) {
        if (!connection.completed) {
          connection.endAll();
        }
      }
    }
  };
  if (hooks.bootstrap.has('server')) {
    hooks.death.addActionAfter('server', 'connections', closeConnections);
  } else {
    hooks.death.addAction('connections', closeConnections);
  }
});

export function createContextNamespace(context: BaseContext, info?: GraphQLResolveInfo, id?: any) {
  let {
    operationName,
    operationType,
  } = context;
  let namespace: string;
  if (typeof info !== 'undefined') {
    if (typeof operationType === 'undefined') {
      operationType = info.operation.operation;
    }
    if (typeof operationName === 'undefined'
      && typeof info.operation.name !== 'undefined') {
      operationName = info.operation.name.value;
    }
    operationType = `${operationType}.${info.fieldName}`;
    if (info.path.key !== info.fieldName) {
      if (typeof operationName === 'undefined') {
        operationName = '';
      }
      operationName = `${operationName}.${info.path.key}`;
    }
  }
  if (typeof operationType !== 'undefined') {
    namespace = `${operationType}`;
  } else {
    namespace = '';
  }
  if (typeof operationName !== 'undefined') {
    namespace = `${namespace}@${operationName}`;
  }
  return `${namespace}::${typeof id !== 'undefined' ? id : context.id}`;
}

export class BaseContext {
  schema: GraphQLSchema;
  event: EventEmitter;
  executionEvent?: EventEmitter;
  execute: ContextExecute;
  subscribe: ContextSubscribe;
  context?: this = undefined;

  public completed: boolean;
  public completionErrors?: ReadonlyArray<InputError>;
  public completionReason?: string;

  public isExecution: false;

  destroy(handler: (...args: any[]) => void) {
    return this.once('destroy', handler);
  }

  on(event: string, handler: (...args: any[]) => void) {
    return this.event.on(event, handler);
  }

  once(event: string, handler: (...args: any[]) => void) {
    return this.event.once(event, handler);
  }

  removeListener(event: string, handler: (...args: any[]) => void) {
    return this.event.removeListener(event, handler);
  }

  removeAllListeners(event: string) {
    return this.event.removeAllListeners(event);
  }

  emit(event: string, ...args: any[]) {
    return this.event.emit(event, ...args);
  }

  constructor(
    public url: UrlWithParsedQuery,
    public headers: http.IncomingHttpHeaders,
    public params: ContextParams,
    public operationName?: string,
    public operationType?: string,
    public id?: any,
  ) {
    this.event = new EventEmitter();
    this.schema = getSchema();
    (<any>this)[$$end] = [];
    this.execute = async (document, variableValues, operationName) =>
      <any>corticalExecute(document, this.url, this.headers, this.params, variableValues, operationName);
    this.subscribe = async (document, variableValues, operationName) =>
      <any>corticalSubscribe(document, this.url, this.headers, this.params, variableValues, operationName);
    Object.defineProperty(this, 'completed', { value: false, configurable: true });
    if (typeof this.id === 'undefined') {
      this.id = createContextId(this);
    }
    connections.add(this);
  }

  log = logger(createContextNamespace(this));
  debug: LogExec<LogLevels> = this.log.debug.bind(this.log);

  execution(info?: GraphQLResolveInfo) {
    return createExecutionContext(this, info);
  }

  endAll(): void
  endAll(errors: InputError | ReadonlyArray<InputError> | null | undefined, reason?: string): void
  endAll(handler: (errors?: ReadonlyArray<InputError>, reason?: string) => void): void
  endAll(value?: any, reason?: string): any {
    if (typeof value === 'function') {
      if (this.completed) {
        process.nextTick(value);
      } else {
        (<any>this)[$$end].push(value);
      }
    } else {
      let self = this;
      if (self.completed) {
        throw new Error('The execution has already ended');
      }
      Object.defineProperty(self, 'completed', { value: true });
      process.nextTick(() => {
        (async () => {
          self.completionReason = reason;
          if (Array.isArray(value)) {
            self.completionErrors = value;
          } else if (value instanceof Error) {
            self.completionErrors = [value];
          }
          const handlers = (<any>self)[$$end].splice(0, (<any>self)[$$end].length);
          await Promise.all(
            handlers
            .map(async (handler: any) => handler(self.completionErrors, self.completionReason))
            .map((promise: Promise<any>) => promise.catch(error => console.log(error.stack))),
          );
          connections.delete(this);
          self.event.emit('destroy', self.completionErrors, this.completionReason);
        })().catch(error => {
          console.log(error.stack);
        });
      });
    }
  }

  end(): void
  end(errors: InputError | ReadonlyArray<InputError> | null | undefined, reason?: string): void
  end(handler: (errors?: ReadonlyArray<InputError>, reason?: string) => void): void
  end(value?: any, reason?: string): any {
    return this.endAll(value, reason);
  }
};

export class BaseExecutionContext<T extends BaseContext> {
  executionEvent = new EventEmitter();
  isExecution = true;
  id: any;
  debug: LogExec<LogLevels>;
  log: Log;

  public completed: boolean;
  public completionError?: Error;
  public completionReason?: string;

  execution(info = this.info) {
    return this.context.execution(info);
  }

  constructor(public context: T, public info?: GraphQLResolveInfo) {
    this.id = createExecutionContextId(context, info);
    const namespace = createContextNamespace(context, info, this.id);
    this.log = logger(`cortical:${namespace}`);
    this.debug = this.log.debug.bind(this.log);
    (<any>this)[$$end] = [];
    if (typeof (<any>this)[$$executions] === 'undefined') {
      (<any>this)[$$executions] = [];
    }
    (<any>this)[$$executions].push(this);
    Object.defineProperty(this, 'completed', { value: false, configurable: true });
    this.context.end((error, reason) => {
      if (!this.completed) {
        this.end(error, reason);
      }
    });
  }

  destroy(handler: (...args: any[]) => void) {
    return this.once('destroy', handler);
  }

  on(event: string, handler: (...args: any[]) => void) {
    return this.executionEvent.on(event, handler);
  }

  once(event: string, handler: (...args: any[]) => void) {
    return this.executionEvent.once(event, handler);
  }

  removeListener(event: string, handler: (...args: any[]) => void) {
    return this.executionEvent.removeListener(event, handler);
  }

  removeAllListeners(event: string) {
    return this.executionEvent.removeAllListeners(event);
  }

  emit(event: string, ...args: any[]) {
    return this.executionEvent.emit(event, ...args);
  }

  end(): void
  end(error: InputError | ReadonlyArray<InputError> | null | undefined, reason?: string): void
  end(handler: (errors?: ReadonlyArray<InputError>, reason?: string) => void): void
  end(value?: any, reason?: string): any {
    if (typeof value === 'function') {
      if (this.completed) {
        process.nextTick(value);
      } else {
        (<any>this)[$$end].push(value);
      }
    } else {
      const self = this;
      if (self.completed) {
        throw new Error('The execution has already ended');
      }
      Object.defineProperty(self, 'completed', { value: true });
      process.nextTick(() => {
        (async () => {
          self.completionReason = reason;
          if (value instanceof Error) {
            self.completionError = value;
          }
          const handlers = (<any>self)[$$end].splice(0, (<any>self)[$$end].length);
          await Promise.all(
            handlers
            .map(async (handler: any) => handler(self.completionError, self.completionReason))
            .map((promise: Promise<any>) => promise.catch(error => console.log(error.stack))),
          );
          self.executionEvent.emit('destroy', self.completionError, this.completionReason);
        })().catch(error => {
          console.log(error.stack);
        });
      });
    }
  }
}

export type ExecutionContext<T extends BaseContext> = T & BaseExecutionContext<T>;

export async function createContext<T extends BaseContext = BaseContext>(url: UrlWithParsedQuery, headers: http.IncomingHttpHeaders, params: ContextParams, operationName?: string, operationType?: string, executionId?: any): Promise<T> {
  const { Context } = require(pathResolve(getApiPath(), 'context'));
  const context: BaseContext = new Context(url, headers, params, operationName, operationType, executionId);
  await contextHook.do({ context, params });
  return <any>context;
}

export async function createExecutionContext<T extends BaseContext = BaseContext>(context: T, info?: GraphQLResolveInfo): Promise<ExecutionContext<T>> {
  const execution = new BaseExecutionContext(context, info);
  await contextExecutionHook.do({ execution, context, info });
  for (const key in context) {
    if (!(key in execution) && typeof (<any>execution)[key] === 'undefined') {
      const contextMethod = <any>context[key];
      (<any>execution)[key] = typeof contextMethod === 'function'
        ? contextMethod.bind(context)
        : contextMethod;
    }
  }
  return <any>execution;
}

export function getOperationFromQuery(query: string) {
  const match = typeof query === 'string'
    ? query.match(/^\s*([^\s]+)(?:\s+([^\s]+))?\s*{/)
    : null;
  if (match != null) {
    return {
      operationName: match[2],
      operationType: match[1],
    };
  }
  return {
    operationName: undefined,
    operationType: undefined,
  };
}

export async function createContextFromExpressRequest<T extends BaseContext = BaseContext>(req: Request, params?: ContextParams) {
  let contextParams = { ...params };
  contextParams = expressRequestContextParamsHook.filter(contextParams, { req, params });
  let operationName: string | undefined;
  let operationType: string | undefined;
  if (req.path === basePath(config.server.graphqlPath)) {
    let query: string | undefined;
    if (req.method === 'POST') {
      query = typeof req.body === 'object' && req.body != null ? req.body.query : undefined;
    } else {
      query = typeof req.query === 'object' && req.query != null ? req.query.query : undefined;
    }
    if (query != null) {
      ({
        operationName,
        operationType,
      } = getOperationFromQuery(query));
    }
  }
  return await createContext<T>(
    urlParse(req.originalUrl, true),
    req.headers,
    contextParams,
    operationName,
    operationType,
  );
}

export async function createContextFromWebSocketTransport<T extends BaseContext = BaseContext>(webSocket: WebSocket, message: OperationMessage, params: ExecutionParams) {
  const upgradeReq: http.IncomingMessage = (<any>webSocket).upgradeReq;
  let contextParams = { ...(typeof message !== 'undefined' && typeof message.payload !== 'undefined' ? message.payload.context : null) };
  contextParams = webSocketTransportContextParamsHook.filter(contextParams, { webSocket, message, params });

  let operationName: string | undefined;
  let operationType: string | undefined;
  if (typeof params.query === 'string') {
    ({
      operationName,
      operationType
    } = getOperationFromQuery(params.query));
  }
  return await createContext<T>(
    urlParse(upgradeReq.url || '', true),
    upgradeReq.headers,
    contextParams,
    operationName,
    operationType,
  );
}
