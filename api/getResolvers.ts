import { GraphQLSchema } from 'graphql';
import { default as resolversHook } from '../hooks/resolvers';
import { getTranspiler } from './getTranspiler';
import { createExecutionContext } from '../api/context';
import { isAsyncIterable } from 'iterall';
import { IResolvers } from 'graphql-tools/dist/Interfaces';

let resolvers: IResolvers;

function mapContextExecution(resolvers: IResolvers) {
  const newResolvers: IResolvers = {};
  const mapResolver = (resolver: any) =>
    (async (rootValue: any, args: any, context: any, info: any) => {
      const execution = await createExecutionContext(context, info);
      try {
        const result = await resolver(rootValue, args, execution, info);
        if (isAsyncIterable(result)) {
          return (async function *() {
            try {
              for await (const data of result) {
                yield data;
              }
            } catch (error) {
              if (!execution.completed) {
                execution.end(error);
              }
              throw error;
            }
          })();
        }
        return result;
      } catch(error) {
        if (!execution.completed) {
          execution.end(error);
        }
        throw error;
      }
    });
  for (const key in resolvers) {
    const resolver = resolvers[key];
    if (typeof resolver === 'object' && resolver != null) {
      newResolvers[key] = {};
      for (const field in resolver) {
        const fieldResolver = (<any>resolver)[field];
        if (typeof fieldResolver === 'function') {
          (<any>newResolvers)[key][field] = mapResolver(fieldResolver);
        } else {
          (<any>newResolvers)[key][field] = {
            ...fieldResolver,
          };
          if (typeof fieldResolver.subscribe === 'function') {
            (<any>newResolvers)[key][field].subscribe = mapResolver(fieldResolver.subscribe);
          }
          if (typeof fieldResolver.resolve === 'function') {
            (<any>newResolvers)[key][field].resolve = mapResolver(fieldResolver.resolve);
          }
        }
      }
    }
  }
  return newResolvers;
}

resolversHook.addFilter('executionContext', mapContextExecution);

export function getResolvers(schema: GraphQLSchema): IResolvers {
  if (resolvers !== undefined) {
    return resolvers;
  }
  const transpiler = getTranspiler();
  resolvers = transpiler.getResolvers(schema);
  resolvers = resolversHook.filter(resolvers, {});
  return resolvers;
}
