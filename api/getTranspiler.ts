import { resolve as pathResolve } from 'path';
import * as appDirRoot from 'app-root-dir';
import { TypeScript2GraphQL } from '@cortical/ts2graphql';
import { default as externalTypesHook } from '../hooks/externalTypes';
import { values } from 'lodash';
import { getApiPath } from '../paths';

let transpiler: TypeScript2GraphQL;
export function getTranspiler() {
  const externalTypes = externalTypesHook.filter([], {});
  if (typeof transpiler === 'undefined') {
    transpiler = new TypeScript2GraphQL({
      paths: {
        root: getApiPath(),
        types: 'types',
        query: 'query',
        mutation: 'mutation',
        subscription: 'subscription',
        scalars: 'scalars',
        validators: 'validators',
      },
      types: externalTypes,
    });
  }
  return transpiler;
}
