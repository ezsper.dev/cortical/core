import * as notifier from 'node-notifier';
import { resolve as pathResolve } from 'path';

export function notify(message: string, options?: notifier.Notification) {
  return notifier.notify(
    {
      ...options,
      message,
      title: 'Cortical',
      icon: pathResolve(__dirname, '..', '..', 'cortical.png'),
      sound: false,
      wait: false,
    }
  );
}
