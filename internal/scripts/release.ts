/**
 * This script builds to distribuition
 */
import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { exec } from '../utils/exec';

function fileFilter(
  target: string,
  filter: (data: string) => string,
  encoding = 'utf8',
) {
  fs.writeFileSync(
    target,
    filter(fs.readFileSync(target).toString(encoding)),
    encoding,
  );
}

// First clear the build output dir.
exec(`rimraf ${pathResolve(appRootDir.get(), 'dist')}`);
// Build typescript
exec('tsc --outDir dist');
// Copy documents
fs.copySync(
  pathResolve(appRootDir.get(), 'README.md'),
  pathResolve(appRootDir.get(), 'dist/README.md'),
);

// Copy bin
fs.copySync(
  pathResolve(appRootDir.get(), 'bin'),
  pathResolve(appRootDir.get(), 'dist/bin'),
);
// replace ts-node for node
fileFilter(
  pathResolve(appRootDir.get(), 'dist/bin/cortical'),
  data => data.replace(/.*ts-node\/register.*/, ''),
);

// Clear our package json for release
const packageJson = require('../../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson['lint-staged'];
delete packageJson.jest;
const { devDependencies } = packageJson;
delete packageJson.devDependencies;

fs.writeFileSync(
  pathResolve(appRootDir.get(), './dist/package.json'),
  JSON.stringify(packageJson, null, 2),
);
// Copy .gitignore
fs.copySync(
  pathResolve(appRootDir.get(), '.gitignore'),
  pathResolve(appRootDir.get(), 'dist/.gitignore'),
);

fs.removeSync(pathResolve(appRootDir.get(), 'dist/sample'));

fs.copySync(
  pathResolve(appRootDir.get(), 'sample'),
  pathResolve(appRootDir.get(), 'dist/sample'),
);

fs.copySync(
  pathResolve(appRootDir.get(), 'cortical.png'),
  pathResolve(appRootDir.get(), 'dist/cortical.png'),
);

const samplePackageJson = require('../../dist/sample/package.json');
samplePackageJson.dependencies['@cortical/core'] = `^${packageJson.version}`;
samplePackageJson.dependencies['@cortical/types'] = `${devDependencies['@cortical/types']}`;
samplePackageJson.dependencies['typescript'] = `${devDependencies['typescript']}`;

fs.writeFileSync(
  pathResolve(appRootDir.get(), 'dist/sample/package.json'),
  JSON.stringify(samplePackageJson, null, 2),
);
