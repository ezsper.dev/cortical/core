/**
 * This script builds a production output of all of our bundles.
 */
import { resolve as pathResolve } from 'path';
import * as appRootDir from 'app-root-dir';
import * as fs from 'fs';
import { builder, handler, Arguments } from './start.yargs';
import build from './build';
import { getBuildPath, getBuildRootPath } from '../../paths';
import { spawn } from 'child_process';
import { notify } from '../utils/notify';

export default async (argv: Arguments) => {
  if (argv.rebuild || !fs.existsSync(getBuildPath())) {
    await build({ _: [], $0: '' });
  }
  const child = spawn('node', [
    '-r',
    'tsconfig-paths/register',
    '-r',
    '@cortical/core',
    getBuildRootPath(),
    '--color',
  ], {
    cwd: getBuildRootPath(),
    env: {
      ...process.env,
      PATH: process.env.PATH,
      NODE_ENV: 'production',
      ...(process.env.CORTICAL_DIR != null && {
        CORTICAL_DIR: process.env.CORTICAL_DIR,
      }),
      CORTICAL_COMMAND: 'start',
    },
  });

  child.stdout.on('data', data => console.log(data.toString().trim()));
  child.stderr.on('data', (data) => {
    console.error(data.toString().trim());
  });

  child.once('exit', (code, signal) => {
    if (code > 0) {
      notify('✖️ An error has occured, check the terminal');
    } else {
      notify('✖️ Server was shutdown abruptly');
    }
  })
}

if (!module.parent) {
  handler(builder().argv)
    .catch(err => {
      console.log(err.stack);
    });
}
