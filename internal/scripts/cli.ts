import '@spine/bootstrap/node';
import * as yargs from 'yargs';
import * as build from './build.yargs';
import * as develop from './develop.yargs';
import * as start from './start.yargs';
import * as create from './create.yargs';

yargs
  .command(
    'build',
    'Build the app',
    build,
  )
  .command(
    'develop',
    'Develop the app schema',
    develop,
  )
  .command(
    'start',
    'Start the app schema',
    start,
  )
  .command(
    'create <name>',
    'Create a sample app',
    create,
  )
  .argv;
