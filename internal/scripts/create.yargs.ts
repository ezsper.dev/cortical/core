import * as Yargs from 'yargs';

export interface Arguments extends Yargs.Arguments {}

export interface Argv extends Yargs.Argv {
  name: string;
  argv: Arguments;
}

export const builder = (yargs: Yargs.Argv = Yargs) =>
  <Argv>yargs;

export const handler = async (argv: Arguments) => {
  await (await import('./create')).default(argv);
}
