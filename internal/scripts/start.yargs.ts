import * as Yargs from 'yargs';

export interface Arguments extends Yargs.Arguments {
  rebuild: boolean;
}

export interface Argv extends Yargs.Argv {
  argv: Arguments;
}

export const builder = (yargs: Yargs.Argv = Yargs) =>
  <Argv>yargs
    .option('rebuild', {
      boolean: true,
      default: false,
    });

export const handler = async (argv: Arguments) => {
  await (await import('./start')).default(argv);
}
