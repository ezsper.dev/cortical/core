/**
 * This script builds a production output of all of our bundles.
 */
import { resolve as pathResolve } from 'path';
import * as appRootDir from 'app-root-dir';
import { builder, handler, Arguments } from './create.yargs';
import { exec } from '../utils/exec';
import { Prompt } from '../utils/Prompt';
import * as fs from 'fs-extra';
import * as readline from 'readline';

export default async (argv: Arguments) => {
  const prompt = new Prompt();

  let name = argv.name;
  let license = 'MIT';

  let answer: string;

  answer = await prompt.ask(`Give the name of your project [${name}]:\n`);
  if (answer !== undefined && answer.trim() !== '') {
    name = answer.trim();
  }

  answer = await prompt.ask(`Give the license of your project [${license}]:\n`);
  if (answer !== undefined && answer.trim() !== '') {
    license = answer.trim();
  }

  prompt.close();

  const projectPath = pathResolve(process.cwd(), argv.name);

  if (fs.existsSync(projectPath)) {
    console.error(`Directory ${argv.name} already exists`);
    process.exit(0);
  }

  fs.copySync(
    pathResolve(__dirname, '..', '..', 'sample'),
    projectPath,
  );

  const packageJsonPath = pathResolve(projectPath, 'package.json');
  const packageJson = JSON.parse(
    fs.readFileSync(packageJsonPath).toString('utf8'),
  );
  packageJson.name = name;
  packageJson.license = license;
  fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 2));
}

if (!module.parent) {
  handler(builder().argv)
    .catch(err => {
      console.log(err.stack);
    });
}
