/**
 * This script builds a production output of all of our bundles.
 */
import { builder, handler, Arguments } from './build.yargs';
import { getSourcePath, getSourceRootPath } from '../../paths';
import * as chokidar from 'chokidar';
import { spawn, ChildProcess } from 'child_process';
import { notify } from '../utils/notify';

export default async (argv: Arguments) => {
  let hasExited = false;
  let hasChanged = false;
  let hadError = false;
  let child: ChildProcess;

  function start() {
    if (child != null) {
      child.removeAllListeners();
      child.stdout.removeAllListeners();
      child.stderr.removeAllListeners();
    }

    child = spawn('node', [
      '-r',
      'ts-node/register',
      '-r',
      'tsconfig-paths/register',
      '-r',
      '@cortical/core',
      getSourceRootPath(),
      '--color',
    ], {
      env: {
        ...process.env,
        PATH: process.env.PATH,
        NODE_ENV: 'development',
        ...(process.env.CORTICAL_DIR != null && {
          CORTICAL_DIR: process.env.CORTICAL_DIR,
        }),
        CORTICAL_COMMAND: 'develop',
        ...(hasChanged && {
          SERVER_RESTART: true,
        })
      }
    });

    hasChanged = false;
    hasExited = false;
    hadError = false;

    child.stdout.on('data', data => console.log(data.toString().trim()));
    child.stderr.on('data', (data) => {
      console.error(data.toString().trim());
    });

    child.once('exit', (code, signal) => {
      if (code > 0) {
        notify('✖️ An error has occured, check the terminal');
        watcher.close();
        process.exit(code);
        return;
      } else if (!hasExited && !hasChanged) {
        notify('✖️ Server was shutdown abruptly');
      }
      if (!hasChanged) {
        watcher.close();
        process.exit(code);
      } else {
        restart();
      }
    })
  }

  function restart() {
    console.log('\nRestarting...\n');
    start();
  }

  const watcher = chokidar.watch('./**/*.ts', {
    cwd: getSourcePath(),
    ignored: ['node_modules'],
  }).on('change', () => {
    console.log('\nThe project has changed...\n');
    hasChanged = true;
    try {
      child.kill();
    } catch (error) {
      console.log(error);
      try {
        process.kill(-child.pid, 'SIGKILL');
      } catch (error) {
        console.log(error);
      }
    }
  });

  start();

  function destroy() {
    hasExited = true;
    watcher.close();
    if (typeof child !== 'undefined') {
      child.kill();
    }
    process.exit(0);
  }

  process.on('SIGINT', destroy);
  process.on('SIGTERM', destroy);
  process.on('SIGUSR2', destroy);
  process.on('exit', destroy);
}

if (!module.parent) {
  handler(builder().argv)
    .catch(err => {
      console.log(err.stack);
    });
}
