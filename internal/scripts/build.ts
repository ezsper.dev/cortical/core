/**
 * This script builds a production output of all of our bundles.
 */
import { resolve as pathResolve } from 'path';
import { builder, handler, Arguments } from './build.yargs';
import { exec } from '../utils/exec';
import { getTranspiler } from '../../api/getTranspiler';
import {
  getProjectPath,
  getSourceRootPath,
  getBuildPath,
  getBuildSchemaPath,
} from '../../paths';
import { default as initHook } from '../../hooks/init';
import * as fs from 'fs-extra';
import { logger } from '@spine/logger';

const log = logger('cortical:build');

require('ts-node/register');
require('tsconfig-paths/register');

process.env.CORTICAL_COMMAND = 'build';

export default async (argv: Arguments) => {
  initHook.addActionAfter('schema', 'build:prepare', () => {
    // First clear the build output dir.
    exec(`rimraf ${getBuildPath()}`);
    log.info('Compiling...');
    exec('tsc');
  });
  initHook.addActionAfter('schema', 'build:completes', () => {
    log.info('Dumping GraphQL schema...');
    const transpiler = getTranspiler();
    transpiler.dumpSchema(getBuildSchemaPath());
    fs.copySync(
      pathResolve(getProjectPath(), './tsconfig.json'),
      pathResolve(getBuildPath(), './tsconfig.json'),
    );
  });
  require(getSourceRootPath());
}

if (!module.parent) {
  handler(builder().argv)
    .catch(err => {
      console.log(err.stack);
    });
}
