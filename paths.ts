import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';

export function getProjectPath() {
  return appRootDir.get();
}

export function getRootPath() {
  switch (process.env.CORTICAL_COMMAND) {
    case 'develop':
    case 'build':
      return getSourceRootPath();
    default:
      return getBuildRootPath();
  }
}

export function getConfigPath() {
  return pathResolve(getRootPath(), 'config');
}

export function getApiPath() {
  return pathResolve(getRootPath(), 'api');
}

export function getSchemaPath() {
  return pathResolve(getRootPath(), 'schema');
}

export function getBuildPath() {
  return pathResolve(getProjectPath(), 'build');
}

export function getBuildRootPath() {
  if (process.cwd() === __dirname) {
    return pathResolve(getBuildPath(), 'sample');
  }
  return getBuildPath();
}

export function getBuildApiPath() {
  return pathResolve(getBuildRootPath(), 'api');
}

export function getBuildConfigPath() {
  return pathResolve(getBuildRootPath(), 'config');
}

export function getBuildSchemaPath() {
  return pathResolve(getBuildPath(), 'schema');
}
export function getSourcePath() {
  return getProjectPath();
}
export function getSourceRootPath() {
  if (process.cwd() === __dirname) {
    return pathResolve(getProjectPath(), 'sample');
  }
  return getProjectPath();
}

export function getSourceApiPath() {
  return pathResolve(getSourceRootPath(), 'api');
}

export function getSourceConfigPath() {
  return pathResolve(getSourceRootPath(), 'config');
}

export function getSourceSchemaPath() {
  return pathResolve(getSourceRootPath(), 'schema');
}