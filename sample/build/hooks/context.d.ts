import { HookAction } from '@spine/hook';
import { BaseContext, ContextParams } from '../api/context';
export interface Params {
    context: BaseContext;
    params: ContextParams;
}
declare const _default: HookAction<void, Params>;
export default _default;
