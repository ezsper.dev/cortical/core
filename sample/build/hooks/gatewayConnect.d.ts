import { HookAction } from '@spine/hook';
import * as WebSocket from 'ws';
import { ConnectionContext } from 'subscriptions-transport-ws';
export interface Params {
    connectionParams: Object;
    webSocket: WebSocket;
    context: ConnectionContext;
}
declare const _default: HookAction<void, Params>;
export default _default;
