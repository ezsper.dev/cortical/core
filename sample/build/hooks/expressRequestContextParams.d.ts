import { HookFilterSync } from '@spine/hook';
import { Request } from 'express';
import { ContextParams } from '../api/context';
export interface Params {
    req: Request;
    params?: {};
}
declare const _default: HookFilterSync<ContextParams, Params>;
export default _default;
