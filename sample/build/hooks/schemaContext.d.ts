import { HookFilter } from '@spine/hook';
import { GraphQLSchema } from 'graphql';
import { BaseContext } from '../api/context';
export interface Params {
    context: BaseContext;
}
declare const _default: HookFilter<GraphQLSchema, Params>;
export default _default;
