import { default as deathHook } from '@spine/bootstrap/hooks/death';
export * from '@spine/bootstrap/hooks/death';
export default deathHook;
