"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const death_1 = require("@spine/bootstrap/hooks/death");
__export(require("@spine/bootstrap/hooks/death"));
exports.default = death_1.default;
//# sourceMappingURL=death.js.map