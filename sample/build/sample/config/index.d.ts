import { ConfigSource as BaseConfigSource } from '@cortical/core/config';
export interface MyConfigSource {
}
export interface ConfigSource extends BaseConfigSource, MyConfigSource {
}
export declare const config: any;
