export declare type Fields = 'name' | 'version' | 'description' | 'dependencies';
export declare class Package {
    name: string;
    version: string;
    description: string;
    dependencies: any;
    constructor(obj: Pick<Package, Fields>);
    getDependencies(): string[];
    getDependencyVersion(args: {
        name: string;
    }): string;
}
