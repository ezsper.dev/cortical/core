/// <reference types="node" />
import { EventEmitter } from 'events';
import { InputError, ExecutionResultDataDefault } from '..';
import * as http from 'http';
import { UrlWithParsedQuery } from 'url';
import { Log, LogLevels, LogExec } from '@spine/logger';
import { Request } from 'express';
import * as WebSocket from 'ws';
import { OperationMessage, ExecutionParams } from 'subscriptions-transport-ws';
import { GraphQLSchema, GraphQLResolveInfo, DocumentNode, ExecutionResult } from 'graphql';
export interface ContextExecute {
    <T extends ExecutionResultDataDefault>(document: DocumentNode, variableValues?: {
        [key: string]: any;
    }, operationName?: string): Promise<ExecutionResult<T>>;
}
export interface ContextSubscribe {
    <T extends ExecutionResultDataDefault>(document: DocumentNode, variableValues?: {
        [key: string]: any;
    }, operationName?: string): Promise<ExecutionResult<T> | AsyncIterator<ExecutionResult<T>>>;
}
export interface ContextParams {
    [key: string]: any;
}
export declare function createContextId(context: BaseContext): any;
export declare function createExecutionContextId(context: BaseContext, info?: GraphQLResolveInfo): any;
export declare function createContextNamespace(context: BaseContext, info?: GraphQLResolveInfo, id?: any): string;
export declare class BaseContext {
    url: UrlWithParsedQuery;
    headers: http.IncomingHttpHeaders;
    params: ContextParams;
    operationName?: string | undefined;
    operationType?: string | undefined;
    id?: any;
    schema: GraphQLSchema;
    event: EventEmitter;
    executionEvent?: EventEmitter;
    execute: ContextExecute;
    subscribe: ContextSubscribe;
    context?: this;
    completed: boolean;
    completionErrors?: ReadonlyArray<InputError>;
    completionReason?: string;
    isExecution: false;
    destroy(handler: (...args: any[]) => void): EventEmitter;
    on(event: string, handler: (...args: any[]) => void): EventEmitter;
    once(event: string, handler: (...args: any[]) => void): EventEmitter;
    removeListener(event: string, handler: (...args: any[]) => void): EventEmitter;
    removeAllListeners(event: string): EventEmitter;
    emit(event: string, ...args: any[]): boolean;
    constructor(url: UrlWithParsedQuery, headers: http.IncomingHttpHeaders, params: ContextParams, operationName?: string | undefined, operationType?: string | undefined, id?: any);
    log: Log<{
        error: number;
        warn: number;
        special: number;
        info: number;
        verbose: number;
        debug: number;
        silly: number;
    }, void | {
        readonly name: string;
        readonly options: import("@spine/logger").LoggerOptions<any>;
        getDisplayName(): string;
        enabled(label: any): boolean;
        child<C extends LogLevels = any>(name: string, options?: import("@spine/logger").LoggerOptions<C> | undefined): Log<C, import("@spine/logger").LoggerMember<any, any>>;
        log: Log<any, any>;
    } | ({
        readonly name: string;
        readonly options: import("@spine/logger").LoggerOptions<any>;
        getDisplayName(): string;
        enabled(label: any): boolean;
        child<C extends LogLevels = any>(name: string, options?: import("@spine/logger").LoggerOptions<C> | undefined): Log<C, import("@spine/logger").LoggerMember<any, any>>;
        log: Log<any, any>;
    } & {
        readonly parent: any;
    })>;
    debug: LogExec<LogLevels>;
    execution(info?: GraphQLResolveInfo): Promise<ExecutionContext<this>>;
    endAll(): void;
    endAll(errors: InputError | ReadonlyArray<InputError> | null | undefined, reason?: string): void;
    endAll(handler: (errors?: ReadonlyArray<InputError>, reason?: string) => void): void;
    end(): void;
    end(errors: InputError | ReadonlyArray<InputError> | null | undefined, reason?: string): void;
    end(handler: (errors?: ReadonlyArray<InputError>, reason?: string) => void): void;
}
export declare class BaseExecutionContext<T extends BaseContext> {
    context: T;
    info?: GraphQLResolveInfo | undefined;
    executionEvent: EventEmitter;
    isExecution: boolean;
    id: any;
    debug: LogExec<LogLevels>;
    log: Log;
    completed: boolean;
    completionError?: Error;
    completionReason?: string;
    execution(info?: GraphQLResolveInfo | undefined): Promise<ExecutionContext<T>>;
    constructor(context: T, info?: GraphQLResolveInfo | undefined);
    destroy(handler: (...args: any[]) => void): EventEmitter;
    on(event: string, handler: (...args: any[]) => void): EventEmitter;
    once(event: string, handler: (...args: any[]) => void): EventEmitter;
    removeListener(event: string, handler: (...args: any[]) => void): EventEmitter;
    removeAllListeners(event: string): EventEmitter;
    emit(event: string, ...args: any[]): boolean;
    end(): void;
    end(error: InputError | ReadonlyArray<InputError> | null | undefined, reason?: string): void;
    end(handler: (errors?: ReadonlyArray<InputError>, reason?: string) => void): void;
}
export declare type ExecutionContext<T extends BaseContext> = T & BaseExecutionContext<T>;
export declare function createContext<T extends BaseContext = BaseContext>(url: UrlWithParsedQuery, headers: http.IncomingHttpHeaders, params: ContextParams, operationName?: string, operationType?: string, executionId?: any): Promise<T>;
export declare function createExecutionContext<T extends BaseContext = BaseContext>(context: T, info?: GraphQLResolveInfo): Promise<ExecutionContext<T>>;
export declare function getOperationFromQuery(query: string): {
    operationName: string;
    operationType: string;
} | {
    operationName: undefined;
    operationType: undefined;
};
export declare function createContextFromExpressRequest<T extends BaseContext = BaseContext>(req: Request, params?: ContextParams): Promise<T>;
export declare function createContextFromWebSocketTransport<T extends BaseContext = BaseContext>(webSocket: WebSocket, message: OperationMessage, params: ExecutionParams): Promise<T>;
