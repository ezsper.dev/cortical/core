"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const url = require("url");
const index_1 = require("../config/index");
function siteUrl(path = '') {
    return `http://${index_1.config.server.host}${basePath(path)}`;
}
exports.siteUrl = siteUrl;
function basePath(path = '') {
    return url.resolve(index_1.config.server.path, path.replace(/^\//, ''));
}
exports.basePath = basePath;
function baseWSPath(path = '') {
    return basePath(url.resolve(index_1.config.server.ws.path, path.replace(/^\//, '')));
}
exports.baseWSPath = baseWSPath;
//# sourceMappingURL=url.js.map