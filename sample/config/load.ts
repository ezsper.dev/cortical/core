/**
 * Project Configuration.
 *
 * NOTE: All file/folder paths should be relative to the project root. The
 * absolute paths should be resolved during runtime by our build internal/server.
 */
import { defaultValues } from '@cortical/core/config/defaultValues';
import { config } from '.';

config.load({
  ...defaultValues,
  // put your custom config values here
});
