import { Resolver } from '@cortical/core';
import { Package } from '../types/Package';
import { Context } from '../context';

export const resolvePackage: Resolver<Package, void, Context> = (args, context) => {
  const packageJson = require('../../package.json');
  const {
    name,
    version,
    description,
    dependencies,
  } = packageJson;

  return new Package({
    name,
    version,
    description,
    dependencies,
  });
};
