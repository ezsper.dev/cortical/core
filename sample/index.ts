import { bootstrap } from '@cortical/core';
import '@cortical/types/bootstrap';
import { config } from './config';
// add your hooks and plugins in between
bootstrap()
  .catch(error => {
    console.log(error.stack !== undefined ? error.stack : error.message);
  });
