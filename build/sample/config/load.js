"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Project Configuration.
 *
 * NOTE: All file/folder paths should be relative to the project root. The
 * absolute paths should be resolved during runtime by our build internal/server.
 */
const defaultValues_1 = require("@cortical/core/config/defaultValues");
const _1 = require(".");
_1.config.load(Object.assign({}, defaultValues_1.defaultValues));
//# sourceMappingURL=load.js.map