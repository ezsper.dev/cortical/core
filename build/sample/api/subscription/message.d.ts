import { Subscriber } from '@cortical/core';
import { Context } from '../context';
export declare const subscribeMessage: Subscriber<string, void, Context>;
