/// <reference types="node" />
import { HookActionSeries } from '@spine/hook';
import { Server } from 'http';
import { Application } from '../server/Application';
export interface Params {
    httpServer: Server;
    api: Application;
}
declare const _default: HookActionSeries<any, Params>;
export default _default;
