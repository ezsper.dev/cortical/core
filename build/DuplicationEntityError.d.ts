import { ApolloError } from 'apollo-server-errors';
export declare class EntityDuplicationError extends ApolloError {
    constructor(entityName: string, criteria: any);
}
