import { GraphQLSchema } from 'graphql';
import { IResolvers } from 'graphql-tools/dist/Interfaces';
export declare function getResolvers(schema: GraphQLSchema): IResolvers;
