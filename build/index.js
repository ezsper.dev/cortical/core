"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
require("@spine/bootstrap/node");
require("./loadConfig");
const hooks = require("./hooks");
exports.hooks = hooks;
const getSchema_1 = require("./api/getSchema");
const config_1 = require("./config");
const apollo_server_errors_1 = require("apollo-server-errors");
const EntityDuplicationError_1 = require("./errors/EntityDuplicationError");
exports.EntityDuplicationError = EntityDuplicationError_1.EntityDuplicationError;
const EntityNotFoundError_1 = require("./errors/EntityNotFoundError");
exports.EntityNotFoundError = EntityNotFoundError_1.EntityNotFoundError;
const InternalServerError_1 = require("./errors/InternalServerError");
exports.InternalServerError = InternalServerError_1.InternalServerError;
const graphql_1 = require("graphql");
const execute_1 = require("./hooks/execute");
const bootstrap_1 = require("./hooks/bootstrap");
const subscribe_1 = require("./hooks/subscribe");
const context_1 = require("./api/context");
const iterall_1 = require("iterall");
__export(require("./api/url"));
__export(require("./paths"));
var graphql_tag_1 = require("graphql-tag");
exports.gql = graphql_tag_1.default;
var graphql_2 = require("graphql");
exports.GraphQLError = graphql_2.GraphQLError;
var bootstrap_2 = require("@spine/bootstrap");
exports.bootstrap = bootstrap_2.bootstrap;
__export(require("apollo-server-errors"));
__export(require("./api/context"));
function getInternalError(error) {
    if (!(error instanceof graphql_1.GraphQLError || error instanceof apollo_server_errors_1.ApolloError)) {
        return error;
    }
    if ((error.originalError instanceof graphql_1.GraphQLError || error.originalError instanceof apollo_server_errors_1.ApolloError)) {
        return getInternalError(error.originalError);
    }
    if (error.originalError instanceof Error) {
        return error.originalError;
    }
    return undefined;
}
hooks.formatError.addFilter('default', (error, { internalError, originalError }) => {
    if (internalError) {
        console.log(internalError.stack != null
            ? internalError.stack
            : internalError.message);
    }
    else if (process.env.NODE_ENV !== 'PRODUCTION') {
        if (originalError != null) {
            console.log(originalError.stack != null
                ? originalError.stack
                : originalError.message);
        }
        else {
            console.log(error.stack != null
                ? error.stack
                : error.message);
        }
    }
    return error;
});
function formatErrors(context, errors, options) {
    if (context != null && !context.completed) {
        context.end(errors);
    }
    return errors.map(error => {
        let formattedError;
        if (error instanceof apollo_server_errors_1.ApolloError) {
            formattedError = error;
        }
        else if (error instanceof graphql_1.GraphQLError) {
            formattedError = apollo_server_errors_1.fromGraphQLError(error);
            if (!(formattedError.originalError instanceof Error)) {
                formattedError.originalError = error;
            }
        }
        else {
            formattedError = new InternalServerError_1.InternalServerError(undefined, error);
        }
        const { originalError } = formattedError;
        const internalError = getInternalError(error);
        return hooks.formatError.filter(formattedError, {
            context,
            originalError,
            internalError,
        });
    });
}
exports.formatErrors = formatErrors;
function formatResponse(context, response, options, end = true) {
    if (typeof response.errors !== 'undefined') {
        response.errors = formatErrors(context, response.errors, options);
    }
    if (context != null) {
        context.event.emit('data', response);
    }
    if (end && context != null && !context.completed) {
        context.end();
    }
    return response;
}
exports.formatResponse = formatResponse;
bootstrap_1.default.addAction('executeDefault', () => {
    execute_1.default.addFilter('default', (result, params) => __awaiter(this, void 0, void 0, function* () {
        if (result != null) {
            return result;
        }
        const { schema, document, rootValue, context, variableValues, operationName, fieldResolver, } = params;
        result = yield graphql_1.execute(schema, document, rootValue, context, variableValues, operationName, fieldResolver);
        return formatResponse(context, result);
    }));
    subscribe_1.default.addFilter('default', (result, params) => __awaiter(this, void 0, void 0, function* () {
        if (result != null) {
            return result;
        }
        const { schema, document, rootValue, context, variableValues, operationName, } = params;
        return yield graphql_1.subscribe(schema, document, rootValue, context, variableValues, operationName);
    }));
});
function executeWithContext(schema, document, rootValue = {}, context, variableValues, operationName, fieldResolver) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const result = yield execute_1.default.filter(undefined, {
                context,
                document,
                variableValues,
                operationName,
                schema,
                rootValue,
                fieldResolver,
            });
            if (result == null) {
                throw new Error(`Empty Execution result`);
            }
            return result;
        }
        catch (error) {
            return formatResponse(context, { errors: [error] });
        }
    });
}
exports.executeWithContext = executeWithContext;
function execute(document, url, headers, params, variableValues, operationName, fieldResolver) {
    return __awaiter(this, void 0, void 0, function* () {
        let operationType;
        if (document.definitions[0].kind === 'OperationDefinition') {
            operationType = document.definitions[0].operation;
        }
        const context = yield context_1.createContext(url, headers, params, operationName, operationType);
        return yield executeWithContext(context.schema, document, {}, context, variableValues, operationName, fieldResolver);
    });
}
exports.execute = execute;
function subscribeWithContext(schema, document, rootValue = {}, context, variableValues, operationName) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const result = yield subscribe_1.default.filter(undefined, {
                schema,
                document,
                rootValue,
                context,
                variableValues,
                operationName,
            });
            if (result == null) {
                throw new Error(`Empty Subscribe result`);
            }
            if (iterall_1.isAsyncIterable(result)) {
                const iterable = result;
                return (function () {
                    return __asyncGenerator(this, arguments, function* () {
                        var e_1, _a;
                        try {
                            try {
                                for (var iterable_1 = __asyncValues(iterable), iterable_1_1; iterable_1_1 = yield __await(iterable_1.next()), !iterable_1_1.done;) {
                                    const data = iterable_1_1.value;
                                    yield yield __await(formatResponse(context, data, {}, false));
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (iterable_1_1 && !iterable_1_1.done && (_a = iterable_1.return)) yield __await(_a.call(iterable_1));
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                            if (!context.completed) {
                                context.end();
                            }
                        }
                        catch (error) {
                            yield yield __await(formatResponse(context, { errors: [error], data: undefined }));
                        }
                    });
                })();
            }
            return formatResponse(context, (yield result));
        }
        catch (error) {
            return formatResponse(context, { errors: [error], data: undefined });
        }
    });
}
exports.subscribeWithContext = subscribeWithContext;
function subscribe(document, url, headers, params, variableValues, operationName) {
    return __awaiter(this, void 0, void 0, function* () {
        let operationType;
        if (document.definitions[0].kind === 'OperationDefinition') {
            operationType = document.definitions[0].operation;
        }
        const context = yield context_1.createContext(url, headers, params, operationName, operationType);
        return yield subscribeWithContext(context.schema, document, {}, context, variableValues, operationName);
    });
}
exports.subscribe = subscribe;
hooks.init.addAction('schema', getSchema_1.getSchema);
switch (process.env.CORTICAL_COMMAND) {
    case 'start':
    case 'develop':
        hooks.bootstrap.addAction('server', () => __awaiter(this, void 0, void 0, function* () {
            const { log } = yield Promise.resolve().then(() => require('./server'));
            if (!hooks.init.has('server')) {
                log.verbose('Server is disabled');
            }
            if (config_1.config.server.ws.disable) {
                hooks.init.disable('subscription');
            }
        }));
        break;
}
//# sourceMappingURL=index.js.map