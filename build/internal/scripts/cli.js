"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("@spine/bootstrap/node");
const yargs = require("yargs");
const build = require("./build.yargs");
const develop = require("./develop.yargs");
const start = require("./start.yargs");
const create = require("./create.yargs");
yargs
    .command('build', 'Build the app', build)
    .command('develop', 'Develop the app schema', develop)
    .command('start', 'Start the app schema', start)
    .command('create <name>', 'Create a sample app', create)
    .argv;
//# sourceMappingURL=cli.js.map