"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This script builds a production output of all of our bundles.
 */
const build_yargs_1 = require("./build.yargs");
const paths_1 = require("../../paths");
const chokidar = require("chokidar");
const child_process_1 = require("child_process");
const notify_1 = require("../utils/notify");
exports.default = (argv) => __awaiter(this, void 0, void 0, function* () {
    let hasExited = false;
    let hasChanged = false;
    let hadError = false;
    let child;
    function start() {
        if (child != null) {
            child.removeAllListeners();
            child.stdout.removeAllListeners();
            child.stderr.removeAllListeners();
        }
        child = child_process_1.spawn('node', [
            '-r',
            'ts-node/register',
            '-r',
            'tsconfig-paths/register',
            '-r',
            '@cortical/core',
            paths_1.getSourceRootPath(),
            '--color',
        ], {
            env: Object.assign({}, process.env, { PATH: process.env.PATH, NODE_ENV: 'development' }, (process.env.CORTICAL_DIR != null && {
                CORTICAL_DIR: process.env.CORTICAL_DIR,
            }), { CORTICAL_COMMAND: 'develop' }, (hasChanged && {
                SERVER_RESTART: true,
            }))
        });
        hasChanged = false;
        hasExited = false;
        hadError = false;
        child.stdout.on('data', data => console.log(data.toString().trim()));
        child.stderr.on('data', (data) => {
            console.error(data.toString().trim());
        });
        child.once('exit', (code, signal) => {
            if (code > 0) {
                notify_1.notify('✖️ An error has occured, check the terminal');
                watcher.close();
                process.exit(code);
                return;
            }
            else if (!hasExited && !hasChanged) {
                notify_1.notify('✖️ Server was shutdown abruptly');
            }
            if (!hasChanged) {
                watcher.close();
                process.exit(code);
            }
            else {
                restart();
            }
        });
    }
    function restart() {
        console.log('\nRestarting...\n');
        start();
    }
    const watcher = chokidar.watch('./**/*.ts', {
        cwd: paths_1.getSourcePath(),
        ignored: ['node_modules'],
    }).on('change', () => {
        console.log('\nThe project has changed...\n');
        hasChanged = true;
        try {
            child.kill();
        }
        catch (error) {
            console.log(error);
            try {
                process.kill(-child.pid, 'SIGKILL');
            }
            catch (error) {
                console.log(error);
            }
        }
    });
    start();
    function destroy() {
        hasExited = true;
        watcher.close();
        if (typeof child !== 'undefined') {
            child.kill();
        }
        process.exit(0);
    }
    process.on('SIGINT', destroy);
    process.on('SIGTERM', destroy);
    process.on('SIGUSR2', destroy);
    process.on('exit', destroy);
});
if (!module.parent) {
    build_yargs_1.handler(build_yargs_1.builder().argv)
        .catch(err => {
        console.log(err.stack);
    });
}
//# sourceMappingURL=develop.js.map