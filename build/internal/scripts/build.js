"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This script builds a production output of all of our bundles.
 */
const path_1 = require("path");
const build_yargs_1 = require("./build.yargs");
const exec_1 = require("../utils/exec");
const getTranspiler_1 = require("../../api/getTranspiler");
const paths_1 = require("../../paths");
const init_1 = require("../../hooks/init");
const fs = require("fs-extra");
const logger_1 = require("@spine/logger");
const log = logger_1.logger('cortical:build');
require('ts-node/register');
require('tsconfig-paths/register');
process.env.CORTICAL_COMMAND = 'build';
exports.default = (argv) => __awaiter(this, void 0, void 0, function* () {
    init_1.default.addActionAfter('schema', 'build:prepare', () => {
        // First clear the build output dir.
        exec_1.exec(`rimraf ${paths_1.getBuildPath()}`);
        log.info('Compiling...');
        exec_1.exec('tsc');
    });
    init_1.default.addActionAfter('schema', 'build:completes', () => {
        log.info('Dumping GraphQL schema...');
        const transpiler = getTranspiler_1.getTranspiler();
        transpiler.dumpSchema(paths_1.getBuildSchemaPath());
        fs.copySync(path_1.resolve(paths_1.getProjectPath(), './tsconfig.json'), path_1.resolve(paths_1.getBuildPath(), './tsconfig.json'));
    });
    require(paths_1.getSourceRootPath());
});
if (!module.parent) {
    build_yargs_1.handler(build_yargs_1.builder().argv)
        .catch(err => {
        console.log(err.stack);
    });
}
//# sourceMappingURL=build.js.map