"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
class Prompt {
    constructor() {
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }
    ask(message) {
        return new Promise((resolve) => {
            this.rl.question(message, (answer) => {
                resolve(answer);
            });
        });
    }
    close() {
        this.rl.close();
    }
}
exports.Prompt = Prompt;
//# sourceMappingURL=Prompt.js.map