/// <reference types="node" />
import * as readline from 'readline';
export declare class Prompt {
    rl: readline.ReadLine;
    constructor();
    ask(message: string): Promise<string>;
    close(): void;
}
