"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const appRootDir = require("app-root-dir");
function exec(command, options) {
    child_process_1.execSync(command, Object.assign({ stdio: 'inherit', cwd: appRootDir.get(), env: Object.assign({ PATH: process.env.PATH }, (options != null && options.env)) }, options));
}
exports.exec = exec;
//# sourceMappingURL=exec.js.map