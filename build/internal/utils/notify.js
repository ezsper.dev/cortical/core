"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const notifier = require("node-notifier");
const path_1 = require("path");
function notify(message, options) {
    return notifier.notify(Object.assign({}, options, { message, title: 'Cortical', icon: path_1.resolve(__dirname, '..', '..', 'cortical.png'), sound: false, wait: false }));
}
exports.notify = notify;
//# sourceMappingURL=notify.js.map