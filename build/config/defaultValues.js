"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Project Configuration.
 *
 * NOTE: All file/folder paths should be relative to the project root. The
 * absolute paths should be resolved during runtime by our build internal/server.
 */
const envVars = require("@spine/config/envVars");
const listenAddress = envVars.string('LISTEN_ADDRESS', '0.0.0.0');
const listenPort = envVars.number('LISTEN_PORT', 1337);
let host;
if (typeof process.env.HOST === 'undefined') {
    if (listenAddress === '0.0.0.0' || listenAddress === '127.0.0.1') {
        host = 'localhost';
    }
    else {
        host = listenAddress;
    }
    if (listenPort !== 80) {
        host = `${host}:${listenPort}`;
    }
}
else {
    host = process.env.HOST;
}
exports.defaultValues = {
    server: {
        listenAddress,
        listenPort,
        host,
        path: '/',
        graphqlPath: '/',
        ws: {
            disable: false,
            path: '/',
            graphqlPath: '/',
        },
        json: {},
        cors: {},
        playground: {
            path: '/',
            disable: false,
            version: 'latest',
        },
    },
};
//# sourceMappingURL=defaultValues.js.map