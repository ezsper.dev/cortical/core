"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_errors_1 = require("apollo-server-errors");
class EntityNotFoundError extends apollo_server_errors_1.ApolloError {
    constructor(entityName, criteria) {
        super(`Could not found "${entityName}" with the given criteria`, 'ENTITY_NOT_FOUND_ERROR', {
            entityName,
            criteria,
        });
    }
}
exports.EntityNotFoundError = EntityNotFoundError;
//# sourceMappingURL=EntityNotFoundError.js.map