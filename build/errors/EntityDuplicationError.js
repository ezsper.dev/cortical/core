"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_errors_1 = require("apollo-server-errors");
class EntityDuplicationError extends apollo_server_errors_1.ApolloError {
    constructor(entityName, criteria) {
        super(`There's is already a record for "${entityName}" with the given criteria`, 'ENTITY_DUPLICATION_ERROR', {
            entityName,
            criteria,
        });
    }
}
exports.EntityDuplicationError = EntityDuplicationError;
//# sourceMappingURL=EntityDuplicationError.js.map