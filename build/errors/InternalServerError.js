"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_errors_1 = require("apollo-server-errors");
class InternalServerError extends apollo_server_errors_1.ApolloError {
    constructor(message, originalError) {
        super(message == null ? InternalServerError.defaultMessage : message);
        if (originalError != null) {
            this.originalError = originalError;
        }
    }
}
InternalServerError.defaultMessage = `An internal server error has occurred`;
exports.InternalServerError = InternalServerError;
//# sourceMappingURL=InternalServerError.js.map