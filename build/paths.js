"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appRootDir = require("app-root-dir");
const path_1 = require("path");
function getProjectPath() {
    return appRootDir.get();
}
exports.getProjectPath = getProjectPath;
function getRootPath() {
    switch (process.env.CORTICAL_COMMAND) {
        case 'develop':
        case 'build':
            return getSourceRootPath();
        default:
            return getBuildRootPath();
    }
}
exports.getRootPath = getRootPath;
function getConfigPath() {
    return path_1.resolve(getRootPath(), 'config');
}
exports.getConfigPath = getConfigPath;
function getApiPath() {
    return path_1.resolve(getRootPath(), 'api');
}
exports.getApiPath = getApiPath;
function getSchemaPath() {
    return path_1.resolve(getRootPath(), 'schema');
}
exports.getSchemaPath = getSchemaPath;
function getBuildPath() {
    return path_1.resolve(getProjectPath(), 'build');
}
exports.getBuildPath = getBuildPath;
function getBuildRootPath() {
    if (process.cwd() === __dirname) {
        return path_1.resolve(getBuildPath(), 'sample');
    }
    return getBuildPath();
}
exports.getBuildRootPath = getBuildRootPath;
function getBuildApiPath() {
    return path_1.resolve(getBuildRootPath(), 'api');
}
exports.getBuildApiPath = getBuildApiPath;
function getBuildConfigPath() {
    return path_1.resolve(getBuildRootPath(), 'config');
}
exports.getBuildConfigPath = getBuildConfigPath;
function getBuildSchemaPath() {
    return path_1.resolve(getBuildPath(), 'schema');
}
exports.getBuildSchemaPath = getBuildSchemaPath;
function getSourcePath() {
    return getProjectPath();
}
exports.getSourcePath = getSourcePath;
function getSourceRootPath() {
    if (process.cwd() === __dirname) {
        return path_1.resolve(getProjectPath(), 'sample');
    }
    return getProjectPath();
}
exports.getSourceRootPath = getSourceRootPath;
function getSourceApiPath() {
    return path_1.resolve(getSourceRootPath(), 'api');
}
exports.getSourceApiPath = getSourceApiPath;
function getSourceConfigPath() {
    return path_1.resolve(getSourceRootPath(), 'config');
}
exports.getSourceConfigPath = getSourceConfigPath;
function getSourceSchemaPath() {
    return path_1.resolve(getSourceRootPath(), 'schema');
}
exports.getSourceSchemaPath = getSourceSchemaPath;
//# sourceMappingURL=paths.js.map