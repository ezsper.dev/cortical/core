import { ApolloError } from 'apollo-server-errors';
export declare class InternalServerError extends ApolloError {
    static defaultMessage: string;
    constructor(message?: string, originalError?: Error);
}
