import { HookFilterSync } from '@spine/hook';
import { GraphQLResolveInfo } from 'graphql';
import {
  BaseContext,
} from '../api/context';

export interface Params {
  context: BaseContext;
  info?: GraphQLResolveInfo;
}

export default new HookFilterSync<any, Params>();
