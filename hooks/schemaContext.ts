import { HookFilter } from '@spine/hook';
import { GraphQLSchema } from 'graphql';
import { BaseContext } from '../api/context';

export interface Params {
  context: BaseContext;
}

export default new HookFilter<GraphQLSchema, Params>();
