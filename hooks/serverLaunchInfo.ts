import { HookFilterSync } from '@spine/hook';

export interface Params {}

export default new HookFilterSync<string[], Params>();
