import { HookFilterSync } from '@spine/hook';
import { Request } from 'express';
import { ContextParams } from '../api/context';

export interface Params {
  req: Request;
  params?: {};
}

export default new HookFilterSync<ContextParams, Params>();
