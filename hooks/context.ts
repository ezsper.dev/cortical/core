import { HookAction } from '@spine/hook';
import {
  BaseContext,
  ContextParams,
} from '../api/context';

export interface Params {
  context: BaseContext,
  params: ContextParams,
}

export default new HookAction<void, Params>();
