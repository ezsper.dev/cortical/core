import { HookAction } from '@spine/hook';
import { GraphQLResolveInfo } from 'graphql';
import {
  BaseContext,
  BaseExecutionContext,
} from '../api/context';

export interface Params {
  execution: BaseExecutionContext<BaseContext>,
  context: BaseContext,
  info?: GraphQLResolveInfo,
}

export default new HookAction<void, Params>();
