import { HookFilterSync } from '@spine/hook';
import { IResolvers } from 'graphql-tools/dist/Interfaces';

export { IResolvers }

export interface Params {}

export default new HookFilterSync<IResolvers, Params>();
