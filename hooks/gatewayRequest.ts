import { HookAction } from '@spine/hook';
import { Request } from 'express';

export interface Params {
  req: Request;
}
export default new HookAction<void, Params>();
