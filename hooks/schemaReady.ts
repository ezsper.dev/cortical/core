import { HookActionSync } from '@spine/hook';
import { GraphQLSchema } from 'graphql';

export interface Params {
  schema: GraphQLSchema,
}

export default new HookActionSync<void, Params>();
