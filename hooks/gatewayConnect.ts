import { HookAction } from '@spine/hook';
import * as WebSocket from 'ws';
import { ConnectionContext } from 'subscriptions-transport-ws';

export interface Params {
  connectionParams: Object;
  webSocket: WebSocket;
  context: ConnectionContext;
}
export default new HookAction<void, Params>();
