import { HookFilterSync } from '@spine/hook';
import { Request } from 'express';
import { ContextParams } from '../api/context';
import * as WebSocket from 'ws';
import { OperationMessage, ExecutionParams } from 'subscriptions-transport-ws';

export interface Params {
  webSocket: WebSocket;
  params: ExecutionParams;
  message: OperationMessage;
}

export default new HookFilterSync<ContextParams, Params>();
