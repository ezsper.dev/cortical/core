import { HookActionSeries } from '@spine/hook';
import { Application } from '../server/Application';

export interface Params {
  api: Application,
}

export default new HookActionSeries<void, Params>();
