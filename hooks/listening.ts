import { HookActionSeries } from '@spine/hook';
import { Server } from 'http';
import { Application } from '../server/Application';

export interface Params {
  httpServer: Server;
  api: Application;
}

export default new HookActionSeries<any, Params>();
