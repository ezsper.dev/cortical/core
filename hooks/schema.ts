import { HookFilterSync } from '@spine/hook';
import { GraphQLSchema } from 'graphql';
import { IResolvers } from './resolvers';

export interface Params {
  resolvers: IResolvers,
}

export default new HookFilterSync<GraphQLSchema, Params>();
