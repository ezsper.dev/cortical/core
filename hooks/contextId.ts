import { HookFilterSync } from '@spine/hook';
import {
  BaseContext,
} from '../api/context';

export interface Params {
  context: BaseContext;
}

export default new HookFilterSync<any, Params>();
