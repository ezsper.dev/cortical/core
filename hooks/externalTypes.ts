import { HookFilterSync } from '@spine/hook';
import { GraphQLNamedType } from 'graphql';

export interface Params {}

export default new HookFilterSync<GraphQLNamedType[], Params>();
