import { HookFilter } from '@spine/hook';
import {
  DocumentNode,
  GraphQLSchema,
} from 'graphql';
import {
  ExecutionResult,
} from '..';
import { BaseContext } from '../api/context';

export interface Params {
  schema: GraphQLSchema,
  document: DocumentNode,
  rootValue: any,
  context: BaseContext,
  variableValues?: {[key: string]: any},
  operationName?: string
}

export default new HookFilter<void | ExecutionResult | AsyncIterator<ExecutionResult>, Params>();
