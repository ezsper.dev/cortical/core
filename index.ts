import '@spine/bootstrap/node';
import './loadConfig';
import * as hooks from './hooks';
import { getSchema } from './api/getSchema';
import { config } from './config';
import {
  ApolloError,
  fromGraphQLError,
} from 'apollo-server-errors';
import {
  EntityDuplicationError,
} from './errors/EntityDuplicationError';
import {
  EntityNotFoundError,
} from './errors/EntityNotFoundError';
import {
  InternalServerError,
} from './errors/InternalServerError';
import {
  DocumentNode,
  execute as graphqlExecute,
  subscribe as graphqlSubscribe,
  ExecutionResult,
  GraphQLSchema,
  GraphQLFieldResolver,
  GraphQLError,
  OperationDefinitionNode,
} from 'graphql';
import { default as executeHook } from './hooks/execute';
import { default as bootstrapHook } from './hooks/bootstrap';
import { default as subscribeHook } from './hooks/subscribe';
import {
  ContextParams,
  BaseContext,
  createContext,
} from './api/context';
import { isAsyncIterable } from 'iterall';
import { UrlWithParsedQuery } from 'url';
import { IncomingHttpHeaders } from 'http';

export {
  hooks,
  ExecutionResult,
  EntityNotFoundError,
  EntityDuplicationError,
  InternalServerError,
}

export * from './api/url';
export * from './paths';
export { default as gql } from 'graphql-tag';
export { GraphQLError } from 'graphql';
export { bootstrap } from '@spine/bootstrap';

export * from 'apollo-server-errors';
export {
  Application,
  Request,
  Response,
  NextFunction,
} from './server';

export {
  ValidatorMap,
  Resolver,
  Subscriber,
  ObservableSubscriber,
  AsyncIteratorSubscriber,
} from '@cortical/ts2graphql';

export * from './api/context';

function getInternalError(error: Error | GraphQLError): Error | undefined {
  if (!(error instanceof GraphQLError || error instanceof ApolloError)) {
    return error;
  }
  if ((error.originalError instanceof GraphQLError || error.originalError instanceof ApolloError)) {
    return getInternalError(error.originalError);
  }
  if (error.originalError instanceof Error) {
    return error.originalError;
  }
  return undefined;
}

export interface ExecutionResultDataDefault {
  [key: string]: any
}

export interface InputExecutionResult<TData = ExecutionResultDataDefault> {
  errors?: ReadonlyArray<InputError>;
  data?: TData;
}

export type InputError = Error | GraphQLError | ApolloError;

hooks.formatError.addFilter('default', (error, { internalError, originalError }) => {
  if (internalError) {
    console.log(
      internalError.stack != null
        ? internalError.stack
        : internalError.message,
    );
  } else if (process.env.NODE_ENV !== 'PRODUCTION') {
    if (originalError != null) {
      console.log(
        originalError.stack != null
          ? originalError.stack
          : originalError.message,
      );
    } else {
      console.log(
        error.stack != null
          ? error.stack
          : error.message,
      );
    }
  }
  return error;
});


export function formatErrors<T extends BaseContext = BaseContext>(
  context: T | undefined,
  errors: ReadonlyArray<InputError>,
  options?: {
    formatter?: Function;
    debug?: boolean;
  },
): ReadonlyArray<ApolloError> {
  if (context != null && !context.completed) {
    context.end(errors);
  }
  return errors.map(error => {
    let formattedError: ApolloError;
    if (error instanceof ApolloError) {
      formattedError = error;
    } else if (error instanceof GraphQLError) {
      formattedError = fromGraphQLError(error);
      if (!(formattedError.originalError instanceof Error)) {
        formattedError.originalError = error;
      }
    } else {
      formattedError = new InternalServerError(undefined, error);
    }
    const { originalError } = formattedError;
    const internalError = getInternalError(error);
    return hooks.formatError.filter(formattedError,{
      context,
      originalError,
      internalError,
    });
  });
}

export function formatResponse<T extends BaseContext = BaseContext>(
  context: T | undefined,
  response: InputExecutionResult,
  options?: {
    formatter?: Function;
    debug?: boolean;
  },
  end = true,
): ExecutionResult {
  if (typeof response.errors !== 'undefined') {
    response.errors = formatErrors(context, response.errors, options);
  }
  if (context != null) {
    context.event.emit('data', response);
  }
  if (end && context != null && !context.completed) {
    context.end();
  }
  return <any>response;
}

bootstrapHook.addAction('executeDefault', () => {
  executeHook.addFilter('default', async (result, params) => {
    if (result != null) {
      return result;
    }
    const {
      schema,
      document,
      rootValue,
      context,
      variableValues,
      operationName,
      fieldResolver,
    } = params;
    result = await graphqlExecute(
      schema,
      document,
      rootValue,
      context,
      variableValues,
      operationName,
      fieldResolver,
    );
    return formatResponse(context, result);
  });

  subscribeHook.addFilter('default', async (result, params) => {
    if (result != null) {
      return result;
    }

    const {
      schema,
      document,
      rootValue,
      context,
      variableValues,
      operationName,
    } = params;

    return await graphqlSubscribe(
      schema,
      document,
      rootValue,
      context,
      variableValues,
      operationName,
    );
  });
});

export async function executeWithContext<T extends ExecutionResultDataDefault>(
  schema: GraphQLSchema,
  document: DocumentNode,
  rootValue: any = {},
  context: BaseContext,
  variableValues?: {
    [key: string]: any;
  },
  operationName?: string,
  fieldResolver?: GraphQLFieldResolver<any, any>,
): Promise<ExecutionResult<T>> {
  try {
    const result = await executeHook.filter(undefined, {
      context,
      document,
      variableValues,
      operationName,
      schema,
      rootValue,
      fieldResolver,
    });
    if (result == null) {
      throw new Error(`Empty Execution result`);
    }
    return <any>result;
  } catch(error) {
    return <any>formatResponse(context, { errors: [error] });
  }
}

export async function execute<T extends ExecutionResultDataDefault>(
  document: DocumentNode,
  url: UrlWithParsedQuery,
  headers: IncomingHttpHeaders,
  params: ContextParams,
  variableValues?: {[key: string]: any},
  operationName?: string,
  fieldResolver?: GraphQLFieldResolver<any, any>,
): Promise<ExecutionResult> {
  let operationType: string | undefined;
  if (document.definitions[0].kind === 'OperationDefinition') {
    operationType = (<OperationDefinitionNode>document.definitions[0]).operation;
  }
  const context = await createContext(url, headers, params, operationName, operationType);
  return await executeWithContext<T>(
    context.schema,
    document,
    {},
    context,
    variableValues,
    operationName,
    fieldResolver,
  );
}

export async function subscribeWithContext<T extends ExecutionResultDataDefault>(
  schema: GraphQLSchema,
  document: DocumentNode,
  rootValue: any = {},
  context: BaseContext,
  variableValues?: {[key: string]: any},
  operationName?: string
): Promise<ExecutionResult<T> | AsyncIterator<ExecutionResult<T>>> {
  try {
    const result = await subscribeHook.filter(undefined, {
      schema,
      document,
      rootValue,
      context,
      variableValues,
      operationName,
    });

    if (result == null) {
      throw new Error(`Empty Subscribe result`);
    }

    if (isAsyncIterable(result)) {
      const iterable = result;
      return (async function* () {
        try {
          for await (const data of iterable) {
            yield <any>formatResponse(context, data, {}, false);
          }
          if (!context.completed) {
            context.end();
          }
        } catch (error) {
          yield formatResponse(context, { errors: [error], data: undefined });
        }
      })();
    }
    return <any>formatResponse(
      context,
      <any>(await result),
    );
  } catch(error) {
    return <any>formatResponse(context, { errors: [error], data: undefined });
  }
}

export async function subscribe<T extends ExecutionResultDataDefault>(
  document: DocumentNode,
  url: UrlWithParsedQuery,
  headers: IncomingHttpHeaders,
  params: ContextParams,
  variableValues?: {[key: string]: any},
  operationName?: string
): Promise<ExecutionResult | AsyncIterator<ExecutionResult>> {
  let operationType: string | undefined;
  if (document.definitions[0].kind === 'OperationDefinition') {
    operationType = (<OperationDefinitionNode>document.definitions[0]).operation;
  }
  const context = await createContext(url, headers, params, operationName, operationType);
  return await subscribeWithContext<T>(context.schema, document, {}, context, variableValues, operationName);
}

hooks.init.addAction('schema', getSchema);

switch(process.env.CORTICAL_COMMAND) {
  case 'start':
  case 'develop':
    hooks.bootstrap.addAction('server', async () => {
      const { log } = await import('./server');
      if (!hooks.init.has('server')) {
        log.verbose('Server is disabled');
      }
      if (config.server.ws.disable) {
        hooks.init.disable('subscription');
      }
    });
    break;
}
